<?php
    $title       = "Prótese Fixa Preço";
    $description = "Em geral, a prótese fixa preço cumpre a função de substituir um ou mais dentes que foram perdidos ou destruídos, em virtude de cáries ou traumatismos, e são fixas na boca.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Em geral, a <strong>prótese fixa preço</strong> cumpre a função de substituir um ou mais dentes que foram perdidos ou destruídos, em virtude de cáries ou traumatismos, e são fixas na boca. Ou seja, são conjuntos de coroas dentais, presas sobre dentes ou implantes, feitas de porcelana.</p>
<p>A REOP, por sua vez, com anos de experiência neste ramo, oferece uma odontologia completa, incluindo a <strong>prótese fixa preço</strong>, tendo o foco no atendimento personalizado, prestativo, atencioso, interpretando o desejo das pessoas com uma linguagem gentil, clara e competente. A qualquer hora do dia, estamos disponíveis para tirar todas as suas dúvidas.</p>
<h2><strong>Tudo sobre </strong><strong>prótese fixa preço:</strong></h2>
<p>Primeiramente, é importante entender que a <strong>prótese fixa preço</strong> é instalada sobre alguns implantes no maxilar ou mandíbula com o objetivo de harmonizar a aparência de quem perdeu vários dentes devido a um trauma ou doenças como cáries e gengivite.</p>
<p>Além da <strong>prótese fixa preço</strong>, existem outros métodos para acabar com a perda dental. No consultório, os dentistas também sugerem a peça parcial, totalmente removível e até a flexível. A solução ideal pode ser o implante dentário se a região sem dentes não for abrangente.</p>
<p>De forma resumida, a <strong>prótese fixa preço</strong>  pode ser confeccionada em diversos tipos de materiais, no entanto, as próteses mais utilizadas atualmente são as de metal com porcelana e as de porcelana com infraestrutura sem metal.</p>
<p>Lembrando que, a <strong>prótese fixa preço</strong> também pode ser feita utilizando apenas um dente como suporte. Ainda há outra alternativa, que é a ponte adesiva. Nesse caso, a prótese é confeccionada com uma estrutura de metal dos dois lados. Consulte um profissional.</p>
<h2><strong>REOP - </strong><strong>prótese fixa preço ideal</strong></h2>
<p>Atualmente, nós somos referência no segmento odontológico, focados em fazer o melhor possível sempre, com conforto, praticidade e cordialidade; otimizar o tempo de atendimento, interpretando o desejo das pessoas com uma linguagem gentil e clara. Além disso, pensando em você, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição e parceria.</p>
<p>Venha conhecer a nossa clínica, um mesmo local com especialidades diversas trabalhando juntas, compartilhando as necessidades de nossos pacientes com profissionais capacitados em várias áreas e com recursos tecnológicos apropriados; sempre considerando o desejo do paciente e sua satisfação ao final do trabalho. Está esperando o que para ligar agora mesmo, tirar todas as suas dúvidas e realizar um orçamento sem compromisso? Nós estamos esperando por você.<br /><strong>Prótese fixa preço </strong>baixo é com a gente!</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>