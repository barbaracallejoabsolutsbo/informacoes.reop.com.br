<?php
    $title       = "Emergência Odontológica em SP";
    $description = "Será um prazer te receber como cliente em nossa clínica. Acesse nosso Whatsapp para que você possa tirar todas as suas dúvidas. Agradecemos seu conato ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>É extremamente necessário que você saiba um lugar de confiança que faça a Emergência Odontológica em SP quando aquela dor de dente inesperada aparece. E na Reop fazemos sempre o possível para transparecer que quando necessário buscar por Emergência Odontológica em SP, nossa clínica seja a melhor opção dentre tantas outras, através de suas e outras experiências conosco. Estamos há 20 anos atuando no ramo odontológico e qualidade em qualquer serviço que nos é designado garantimos que não irá lhe faltar. Possuímos equipamentos para qualquer tipo de urgência e profissionais que estão preparados e adaptados para lidar com qualquer tipo de situação em nossa clínica. Nossos meios de contato coo Whatsapp é de fácil acesso para que você nos consulte no momento em que precisar, de onde estiver; até o momento de sua chegada a nossa clínica.  </p>
<p>      Através do nosso site você verá que a Reop é a melhor clínica quando precisar de uma Emergência Odontológica em SP, pois mostramos que possuímos diversos tipos de tratamentos e que independente de sua emergência, conseguiremos lhe atender. Caso seja necessário o seu retorno em nossa clínica, manteremos todo o contato necessário, mandando também e-mail lembrete de checkup para que façamos todo o acompanhamento necessário à sua necessidade. Quando precisar de Emergência Odontológica em SP, nossos profissionais estão dispostos para te receber. Profissionais os quais só exercem suas devidas profissões e especialidades pois possuem diplomas para tal. Nossa prioridade sempre é fazer com que as suas emergências sejam atendidas da forma mais atenciosa possível, por isso possuímos salas de atendimento individualizadas para que no momento em que você é atendido, você seja nosso único foco pois ao longo dos nossos anos de experiência, aprimoramos nosso atendimento e serviço para então melhorarmos sua qualidade de vida em relação a sua saúde bucal. Assim que o seu tratamento for concretizado em nossa clínica, fazemos a questão que nosso contato não se encerre. Estaremos sempre dispostos a te receber em nossa clínica, seja com urgência ou não.  </p>

<h2>Consulte mais detalhes sobre Emergência Odontológica em SP </h2>
<p>Contamos com diversos profissionais para que você possa ser atendido de forma única em nossa cínica. Seja precisando de Emergência Odontológica em SP, ou somente para uma avaliação ou algum tipo de tratamento. Estamos há mais de 15 anos fazendo o trabalho de mais qualidade que você encontrará.  </p>

<h2>A melhor opção para Emergência Odontológica em SP  </h2>
<p>Queremos sempre ser sua primeira opção quando precisar de uma emergência odontológica e por isso todos os nossos meios de contato estão de fácil acesso em nosso site para que você possa nos consultar a qualquer momento  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>