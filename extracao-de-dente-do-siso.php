<?php
    $title       = "Extração de dente do siso";
    $description = "Faça suas perguntas à nossa clínica sem sair de casa! Nosso Whatsapp está disponível para mantermos o contato quando desejar. Será um prazer te atender!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O incômodo da dor do siso é algo inexplicável que só quem já vivenciou sabe como ela é. Portanto ao perceber que eles já se enraizaram em sua boca é extremamente necessário que você faça a Extração de Dente do Siso, para não conviver com essa dor desnecessariamente. Na clínica Reop, há profissionais que atuam nesse meio há longos anos para que ao decorrer desse procedimento tão delicado, você não se preocupe durante e após a Extração de dente do siso. Nossa prioridade sempre é dar total atenção aos nossos pacientes, nas nossas salas de atendimentos individualizados, para que o procedimento não ocorra de forma dolorida.  </p>
<p>    Fazemos questão de sempre manter contato até mesmo após a Extração de Dente do Siso, para que nossos pacientes possam manter todo o cuidado fundamental nessa operação. Enviamos e-mail lembrete de checkup e mantemos contato pelo Whatsapp, para que caso haja alguma dúvida, ou o paciente queira voltar em nossa clínica, ser de fácil constatação. </p>
<p>Sabemos que a Extração de dente do Siso é algo indispensável, por isso apresentamos preços acessíveis e diversas formas de pagamento, como o parcelamento, para que todos que precisam tenham acesso a esse serviço. Não deixe de nos consultar para tirar suas devidas dúvidas em relação a valores ou até mesmo sobre a Extração de dente do siso. Nossa equipe está sempre à disposição de nossos pacientes, para que vocês possam ser atendidos da maneira mais respeitosa e atenciosa possível. Todos os profissionais da Reop estão altamente trinados e qualificados para receber cada um em nossa clínica. Temos o objetivo de fazer com que ao entrar em nossa clínica, você se surpreenda de forma positiva com o nosso trabalho  </p>

<h2>Mais detalhes sobre Extração de dente do Siso  </h2>
<p>Temos uma equipe preparada para te atender tanto virtualmente, quanto presencialmente. Não deixe de nos procurar quando precisar fazer sua Extração de Dente do Siso, nossos profissionais estão aptos para que esse procedimento seja feito da forma mais cautelosa possível. Será um prazer recebê-lo como cliente. Estamos sempre à disposição e abertos para novas sugestões que nos forem propostas. </p>
<h2> </h2>
<h2>Consulte a melhor opção para Extração de Dente do Siso </h2>
<p>Estamos dispostos a sanar todas as suas dúvidas perante a nossa clínica. Não deixe de fazer seu orçamento conosco, você pode nos solicitar através de nosso Whatsapp ou até mesmo nos visitar em nossa clínica. Para mais detalhes sobre nossos meios de contato, consulte-os em nosso site  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>