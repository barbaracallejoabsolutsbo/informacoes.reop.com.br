<?php
    $title       = "Implante de Dentes de Resina";
    $description = "Para colocar um implante de dentes de resina é necessário fixar um pino no osso alveolar, onde anteriormente ficava o dente original, através de uma pequena cirurgia realizada na própria clínica. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A REOP é uma clínica odontológica e estética localizada na Avenida Paulista. Com fácil acesso, preços surpreendentes e toda qualidade de uma excelente clínica, aqui é possível encontrar tratamentos completos para reabilitação bucal, <strong>implante de dentes de resina</strong>, porcelana, facetas, lentes de contato e muito mais. Acesse nosso site e conheça mais sobre nossos tratamentos e procedimentos estéticos especializados.</p>
<p>Se você precisa substituir um dente extraído ou que foi perdido, quer melhorar a estética do seu sorriso e reconquistar sua mastigação completa, o <strong>implante de dentes de resina,</strong> ou de porcelana, podem ser a melhor solução para você. O procedimento cirúrgico é muito simples, com rápida recuperação e sem complicações.</p>
<p>Para colocar um <strong>implante de dentes de resina</strong> é necessário fixar um pino no osso alveolar, onde anteriormente ficava o dente original, através de uma pequena cirurgia realizada na própria clínica. Este pino, normalmente feito em titânio, possui material com biocompatibilidade que cria um tecido que se une a cavidade óssea oferecendo maior sustentação para o implante que irá ser fixado nesta base.</p>
<p>O procedimento é feito basicamente de forma igual para todos os pacientes, com mínimas variações em técnicas para o implante. Voltando o <strong>implante de dentes de resina</strong>, é feito uma prótese que vai substituir o dente perdido, que é fixada sob o pino de titânio, que foi previamente instalado na cavidade óssea a partir de uma pequena cirurgia em nosso consultório. Dessa forma, é feito sob medida, para cada pessoa, os dentes perdidos escolhidos para serem repostos em resina ou porcelana, por exemplo.</p>
<h2><strong>Danificou sua coroa dentária de resina? Restaure seu implante de dentes de resina aqui!</strong></h2>
<p>Com a REOP Odontologia e Estética você pode fazer restaurações completas dos seus dentes e implantes. Instale seu<strong> implante de dentes de resina,</strong> do zero, desde a cirurgia de fixação do pino até a confecção das coroas e próteses. Em nossa clínica, trabalhamos com total atenção e cuidado para superar as expectativas iniciais de cada paciente e satisfazer por completo em todos os tratamentos e procedimentos aqui realizados. Agende já sua consulta para uma avaliação profissional e encontre o tratamento ou procedimento estético que você procura.</p>
<h2><strong>Perdeu um ou mais dentes e quer reconquistar o seu sorriso? Implante de dentes de resina!</strong></h2>
<p>Uma das melhores soluções da atualidade. O<strong> famoso implante de dentes de resina</strong>, ou de material de porcelana, podem reabilitar completamente seu sorriso e elevar sua auto estima de forma incrível. Conheça a REOP Odontologia e Estética, serviços especializados para todo o ramo em nossa clínica na Avenida Paulista.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>