<?php
$title       = "Dentista Urgente em Taboão - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está procurando por serviços de Dentista Urgente em Taboão - Guarulhos, a REOP Odontologia e Estética tem a solução para você. Se você está sentindo alguma dor ou precisa de algum procedimento de restauração com urgência você encontrou o lugar correto. Localizados na Avenida Paulista em São Paulo, temos um valor acessível para consultas e atendimentos, entre em contato com nossa equipe para mais informações.</p>
<p>Especialista no mercado, a REOP ODONTO é uma empresa que ganha visibilidade quando se trata de Dentista Urgente em Taboão - Guarulhos, já que possui mão de obra especializada em Estética Dental Preço, Tratamento de Endodontia preço, Facetas de Resina, Emergência de Canal e Extração de dente do siso preço. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de DENTISTA, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>