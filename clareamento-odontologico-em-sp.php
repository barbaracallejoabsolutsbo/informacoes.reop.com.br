<?php
    $title       = "Clareamento Odontológico em SP";
    $description = "Consulte as avaliações e conclua que oferecemos as melhores condições e benefícios para que você realize clareamento odontológico em SP.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura pelo melhor lugar para realizar <strong>clareamento odontológico em SP </strong>com os melhores preços da região, encontrou o local ideal para buscar esse procedimento. A REOP odontologia e estética é uma clínica dentre as mais conceituadas no Brasil dentro do segmento. Nosso <strong>clareamento odontológico em SP </strong>é oferecido com 3 opções sendo elas o clareamento dental monitorado, clareamento a laser e clareamento híbrido. O monitorado é um procedimento que vai funcionando dia após dia com o uso diário de um gel clareador colocados nas moldeiras disponibilizadas pelo dentista, o clareamento à laser é realizado através de uma máquina laser no consultório dentário e é relativamente mais rápido de ser alcançado o resultado, já o tratamento híbrido é a junção dos dois modelos dentro de um tratamento planejado especificamente para aquele paciente. Tenha dentes mais brancos e sorria sem medo, após passar por algumas sessões já é possível perceber os resultados do tratamento.</p>
<p>Nossos profissionais são capacitados e certificados para realizar todos os tipos de procedimentos oferecidos em nosso catálogo. Nossa missão é estimular e promover a qualidade de vida, cuidando e otimizando o dia a dia de nossos clientes dentro dos segmentos odontológicos e estéticos. Os tratamentos estéticos são procedimentos que vem tomando força a cada dia mais no mercado e a estética dentária é uma das mais procuradas. O nosso <strong>clareamento odontológico em SP </strong>é um procedimento que realmente pode mudar a estética total do seu sorriso. É muito comum o desgaste por conta de alimentos com corante, cigarro ou até mesmo por uma má escovação. Ao realizar nosso <strong>clareamento odontológico em SP </strong>é muito importante que os cuidados com seus dentes sejam reforçados para que não voltem a escurecer e amarelar.</p>
<h2><strong>O melhor lugar para realizar clareamento odontológico em SP.</strong></h2>
<p>Consulte as avaliações e conclua que oferecemos as melhores condições e benefícios para que você realize <strong>clareamento odontológico em SP. </strong>Nossa clínica trabalha com muito empenho e transparência para atender todos os clientes de modo satisfatório e claro.</p>
<h2><strong>Saiba mais sobre nosso clareamento odontológico em SP.</strong></h2>
<p>Para saber mais sobre o <strong>clareamento odontológico em SP</strong> promocional ou quaisquer outros tratamentos odontológicos e estéticos oferecidos por nossa clínica consulte os artigos disponíveis em nosso blog ou entre em contato para ser atendido e auxiliado por um especialista da nossa equipe.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>