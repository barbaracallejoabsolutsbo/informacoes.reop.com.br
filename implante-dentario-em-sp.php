<?php
    $title       = "Implante Dentário em SP";
    $description = "O implante dentário em SP realizado pela REOP pode contar com algumas fases comuns, que são padrões para praticamente todos pacientes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A REOP, Odontologia e Estética, oferece serviços especializados em uma clínica localizada em São Paulo, na avenida Paulista. Com ótimo acesso, preço acessível e serviços exclusivos, oferecemos desde reabilitação oral até procedimentos puramente estéticos. Com nossa clínica, você encontra tratamento completo para <strong>implante dentário em SP</strong> com profissionais excelentes e muita atenção. Fale conosco já e agende um atendimento com nossos profissionais.</p>
<p>O <strong>implante dentário em SP</strong> pode ser feito em nosso consultório localizado no coração da Avenida Paulista. Fale conosco e agende já sua consulta para avaliação profissional das condições para instalação cirúrgica do implante de titânio para fixar as próteses que irão repor dentes perdidos ou extraídos. Encontre mais uma infinidade de tratamentos e procedimentos para reabilitação oral e estética.</p>
<p>O <strong>implante dentário em SP</strong> realizado pela REOP pode contar com algumas fases comuns, que são padrões para praticamente todos pacientes. Podem ser feitos um ou mais dentes em diversos tipos de próteses diferentes, que fixam unitariamente um ou alguns dentes perdidos, ou todos como no caso da prótese protocolo.</p>
<p>A primeira fase do processo de <strong>implante dentário em SP</strong> é a fase cirúrgica, onde é implantado um pino de titânio dentro do osso para que ocorra integração óssea. Esse prazo de integração óssea aumenta a fixação e firmeza do implante, podendo variar de 2 a 6 meses dependendo do estado atual da cavidade óssea do paciente. A próxima etapa é a fase protética, onde é feita a exposição da parte do pino para moldagem do implante para confecção da coroa e instalação, podendo demorar em média 4 consultas.</p>
<h2><strong>Qual diferença entre o implante dentário em SP feito de resina para o de porcelana?</strong></h2>
<p>A base do implante que é fixada no osso é feita do mesmo material, o que muda, principalmente, são os materiais utilizados para confecção dos implantes. O tempo de confecção e dificuldade para manipular o material, a durabilidade, tonalidade da cor ao longo do tempo e a necessidade de polimentos para restauração do brilho são fatores que influenciam diretamente no custo dos implantes de acordo com o material escolhido. O <strong>implante dentário em SP</strong> é mais barato e proporciona um resultado tão eficiente quanto os de porcelana. Conquiste novamente seu sorriso de forma saudável e harmoniosa.</p>
<h2><strong>Implante dentário em SP é na REOP Odontologia</strong></h2>
<p>Próteses exclusivas para melhor atender cada caso de <strong>implante dentário</strong> <strong>em SP</strong>, consulte já os atendentes da REOP Odontologia e Estética e agende sua consulta.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>