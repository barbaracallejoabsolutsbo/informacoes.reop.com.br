<?php
$title       = "Invisalign no Jardim Paulistano";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Invisalign no Jardim Paulistano é uma tecnologia atual no mercado da odontologia e ortodontia. Trata-se de um inovador alinhador ortodôntico transparente, em formato de placa, muito similar a placas para bruxismo. É um tratamento alternativo e muito discreto, além de ser removível. O tratamento se baseia em um conjunto de placas e moldeiras removíveis praticamente invisíveis que devem ser usadas durante o tratamento.</p>
<p>Se está procurando por Invisalign no Jardim Paulistano e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a REOP ODONTO é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de DENTISTA conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Odontopediatria Preço, Aparelho Dental Preço, Aparelho Ortodôntico Auto ligado Preço, Aparelho ortodôntico invisível e Aparelho Odontológico.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>