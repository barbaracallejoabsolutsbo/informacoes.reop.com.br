<?php
    $title       = "Estética Dental Preço";
    $description = "Nossos meios de contato estão disponíveis para que você possa nos constatar para tirar suas devidas dúvidas sobre nossos procedimentos. Não deixe de entrar em contato conosco";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Pra você que está insatisfeito com a forma em que os seus dentes se encontram, Estética dental preço baixo é a melhor opção para você. Aqui na clínica Reop atuamos nesse ramo há mais de 15 anos, com o objetivo de sempre melhorar na autoestima de nossos pacientes. Os profissionais de nossa clínica, possuem grandes experiências em variadas especialidades odontológicas e aprimoraram suas habilidades com seus anos de experiência. Ao procurar por Estética Dental Preço acessível, deduzimos que você queira poder viver uma vida sem a vergonha de até mesmo sorrir. E aqui na clínica Reop, durante o tratamento fazemos a questão de fazer com que a atenção de nosso profissional seja voltada somente a você, respeitando a sua privacidade em nossas salas de atendimento individual, para que assim que necessário buscar uma clínica, você não pensar em outra Clínica odontológica, a não ser a Reop, onde buscamos fazer com que a oportunidade de ter um sorriso cada vez melhor seja acessível para todos aqueles que buscam.   </p>
<p>Não deixe de fazer seu orçamento conosco, pois na Reop garantimos ter uma Estética Dental Preço acessível. Estamos há 20 anos aprimorando nossos conhecimentos para que cada paciente saia de nossa clínica diferente da forma que entrou; tanto esteticamente quanto em questão da sua satisfação. Durante todo o procedimento do tratamento, mantemos contato com nossos pacientes através do WhatsApp para que mesmo de suas casas, consigam nos consultar e tirar suas devidas dúvidas. Também enviamos e-mail lembrete de checkup para mantermos nosso compromisso até o fim. Possuímos equipamentos renomados para que o nosso trabalho se concretize com excelência, através dos nossos profissionais. Priorizamos que nossos profissionais façam seus trabalhos com agilidade, para que você tenha o seu resultado o mais rápido possível, porém não deixando a qualidade do serviço de lado. Desde 2001 visamos aprimorar nossas habilidades para que possamos atender as expectativas de nossos pacientes. Estética Dental Preço baixo, sempre foi uma especialidade presente em nossa clínica, junto com a nossa preferência em elevar a autoestima de cada um daqueles que tornam-se nossos clientes  </p>
<h2>Mais detalhes sobre Estética Dental Preço acessível </h2>
<p>Não pense que a estética de seus dentes é algo fora da sua realidade e impossível de conquistar. Consulte nossa equipe para fazer um orçamento para Estética  Dental acessível preço baixo.  </p>

<h2>A melhor opção para Estética Dental Preço baixo </h2>
<p>Contamos com profissionais qualificados para exercerem suas devidas funções, junto com os melhores materiais para que possamos atender todos os seus desejos. Nossa equipe está prontamente disponível para te atender e tirar todas as suas dúvidas em relação a Estética Dental Preço baixo. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>