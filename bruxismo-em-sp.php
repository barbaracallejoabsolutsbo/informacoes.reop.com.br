<?php
    $title       = "Bruxismo em SP";
    $description = "O bruxismo em SP é um hábito que causa sintomas como dores de cabeça, dores no maxilar, dentes moles, desgaste dos dentes e até mesmo a quebra deles.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura pelo melhor lugar para tratar o <strong>bruxismo em SP </strong>encontrou um dos locais mais recomendados do Brasil. A REOP é uma clínica odontológica e estética que busca oferecer tratamentos modernos, mais eficazes e com menos incômodos para nossos clientes com preço acessível e condições que cabem no seu bolso. O <strong>bruxismo em SP </strong>é uma condição que afeta milhares de pessoas sem ao menos perceber. É muito comum que durante o trabalho, estudos, dormindo ou em qualquer momento em que você esteja concentrado a contração involuntária dos músculos do maxilar fazendo assim com que seus dentes sejam pressionados. Para tratar esse ou qualquer outro tipo de quadro odontológico você pode procurar nossa clínica quando quiser. Nossos profissionais são altamente capacitados e estão sempre a disposição para atender com simpatia e clareza todos os clientes que busquem pelos tratamentos oferecidos em nosso catálogo de serviços.</p>
<p>O <strong>bruxismo em SP </strong>é um hábito que causa sintomas como dores de cabeça, dores no maxilar, dentes moles, desgaste dos dentes e até mesmo a quebra deles. Essa condição é causada por uma desordem funcional provavelmente relacionada a fatores genéticos. Para tratar o <strong>bruxismo em SP </strong>é utilizado normalmente uma placa de modelação dentária que durante a noite ou nos períodos em que a pessoa percebe que o hábito ocorre seja realizada uma proteção dos dentes para não prejudicar ou diminuir os impactos causados por essa condição. Essa condição não tem cura e deve ser tratada sempre para prevenir dores, tensão e desgaste dos dentes e ossos da mandíbula. Não perca tempo e agende agora mesmo sua avaliação com um profissional de nossa clínica. Ficamos localizados na Avenida Paulista 1499 Salas 23 e 24.</p>
<h2><strong>O melhor lugar para tratar bruxismo em SP.</strong></h2>
<p>Consulte as avaliações e conclua que somos um dos melhores lugares para que você possa tratar <strong>bruxismo em SP </strong>com preços acessíveis e condições de parcelamento que somente uma empresa referência dentro do segmento pode oferecer para você.</p>
<h2><strong>Saiba mais sobre o tratamento para bruxismo em SP.</strong></h2>
<p>Para saber mais sobre o nosso tratamento para<strong> bruxismo em SP </strong>ou quaisquer outros procedimentos estéticos e odontológicos oferecidos por nossa clínica consulte nosso blog ou entre em contato e seja atendido por um especialista para te auxiliar e tirar suas dúvidas da melhor maneira possível. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>