<?php
$title       = "Invisalign na Vila Barros - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A resposta é sim, esse tratamento funciona sim. Apesar de seu custo relativamente superior ao tratamento com brackets convencionais, esse tratamento é bem cômodo e discreto. Podendo ver resultados ainda mesmo nos primeiros meses, o tratamento dura de doze a dezoito meses e tem excelentes resultados. Encontre tratamento completo com aparelho ortodôntico Invisalign na Vila Barros - Guarulhos na REOP Odontologia.</p>
<p>Como uma empresa de confiança no mercado de DENTISTA, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Invisalign na Vila Barros - Guarulhos. A REOP ODONTO vem crescendo e mostrando seu potencial através de Consultório Odontológico que Faz Bichectomia, Aparelho Odontológico, Extração de dente do siso, Aparelho ortodôntico invisível e Prótese Dentária Preço, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>