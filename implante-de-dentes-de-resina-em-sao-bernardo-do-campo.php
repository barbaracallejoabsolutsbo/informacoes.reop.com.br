<?php
$title       = "Implante de Dentes de Resina em São Bernardo do Campo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Caso você precise substituir um dente perdido, para fins estéticos e funcionais, hoje temos opções de Implante de Dentes de Resina em São Bernardo do Campo ou de porcelana. Ambos materiais podem substituir um dente ou até mesmo todos. É fixado um pino no osso alveolar onde o implante dentário em si é fixado posteriormente. É necessário fazer alguns exames para saber sobre a possibilidade de fazer um procedimento seguro e bem sucedido.</p>
<p>Contando com profissionais competentes e altamente capacitados no ramo de DENTISTA, a REOP ODONTO oferece a confiança e a qualidade que você procura quando falamos de Extração de dente do siso, Harmonização Facial Especializada, Clareamento Odontológico Preço, Clínica de Ortodontia e Implante Odontológico. Ainda, com o mais acessível custo x benefício para quem busca Implante de Dentes de Resina em São Bernardo do Campo, uma vez que, somos a empresa que mais se desenvolve no mercado, mantendo o melhor para nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>