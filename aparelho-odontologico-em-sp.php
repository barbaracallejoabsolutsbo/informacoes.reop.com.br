<?php
    $title       = "Aparelho Odontológico em SP";
    $description = "Encontre aparelho odontológico em SP com as melhores condições da região que somente um consultório renomado pode oferecer para seus clientes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Encontre <strong>aparelho odontológico em SP </strong>com as melhores condições da região que somente um consultório renomado pode oferecer para seus clientes. A REOP é uma das mais conceituadas clínicas de odontologia e estética do Brasil. Localizados na Avenida Paulista, um dos berços comerciais mais movimentados de São Paulo, realizamos atendimento para diversas pessoas que transitam pelo local cotidianamente. Nosso local conta com duas salas espaçosas para receber nossos clientes com conforto e segurança. Além de <strong>aparelho odontológico em SP </strong>você encontra diversos outros tratamentos odontológicos e estéticos como clareamento, botox, aplicação de enzima e muito mais. São diversas opções de tratamentos estéticos e odontológicos para que atendam todas as suas necessidades em um só lugar. Possuímos a meta de continuar sendo referência no segmento odontológico fazendo sempre o melhor possível com conforto, praticidade e cordialidade com nossos clientes. O melhor lugar para tratamento estético e odontológico em São Paulo consulte as avaliações e aumente ainda mais sua confiabilidade em nossa clínica.</p>
<p>Além de <strong>aparelho odontológico em SP </strong>você será avaliado e encontrará um plano completo de tratamento odontológico de acordo com as necessidades que seu exame apresentar através de nossa avaliação com câmera intra-oral para obter melhores resultados. Prezamos por oferecer um bom relacionamento com nossos clientes com explicações claras e didáticas para que entendam toda a explicação referente aos procedimentos a serem realizados. O sorriso é a fachada do seu humano, e um dos principais pontos a se chamar atenção em uma pessoa. Tendo em vista essa informação é muito comum que algumas pessoas tenham problemas de autoestima por conta de sua estética bucal. Cuide da sua saúde e estética e volte a sorrir sem medo. Agende agora mesmo sua avaliação para colocar <strong>aparelho odontológico em SP. </strong></p>
<h2><strong>O melhor lugar para colocar aparelho odontológico em SP.</strong></h2>
<p>Consulte as avaliações e fique ainda mais tranquilo ao colocar seu <strong>aparelho odontológico em SP </strong>conosco. A REOP é uma clínica altamente conceituada que fica em localização privilegiada na Avenida Paulista, um dos centros comerciais mais movimentados e de fácil acesso da cidade de São Paulo.</p>
<h2><strong>Saiba mais sobre o nosso aparelho odontológico em SP.</strong></h2>
<p>Para saber mais sobre o <strong>aparelho odontológico em SP </strong>da REOP entre em contato e seja atendido por um especialista para te auxiliar e tirar todas suas dúvidas quanto aos nossos tratamentos estéticos e odontológicos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>