<?php
$title       = "Implante Odontológico em Capelinha - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os tipos de próteses que são fixadas no implante odontológica normalmente são em porcelana ou resina, por se assemelhar aos dentes normais, serem resistentes e de boa dureza para uso cotidiano, mas também podem ser encontrados em outros materiais como ouro, prata, níquel, entre outros. Encontre o material mais indicado para seu caso e adquira implantes odontológicos com o melhor consultório odontológico.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Implante Odontológico em Capelinha - Guarulhos? A REOP ODONTO é o que você precisa! Sendo uma das principais empresas do ramo de DENTISTA consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Emergência Odontológica, Lente de Porcelana, Facetas de Resina, Tratamento de Endodontia e Estética Dental Preço. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>