<?php
    $title       = "Lente de Porcelana em SP";
    $description = "Se você está procurando por lente de porcelana em SP, a REOP Odontologia é uma clínica especializada em procedimentos estéticos e clínicos para todo ramo de odontologia.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está procurando por <strong>lente de porcelana em SP</strong>, a REOP Odontologia é uma clínica especializada em procedimentos estéticos e clínicos para todo ramo de odontologia. Conheça nossos serviços exclusivos e agende sua consulta com nossos profissionais. Com fácil localização, estamos no centro da Avenida Paulista, com ótimo acesso e simples de encontrar. Fale com nossos atendentes e marque um horário conosco.</p>
<p>Dentes amarelos, tortos, com anatomia mal definida ou quebrados certamente podem incomodar na sua estética bucal. Conheça todos os procedimentos e tratamentos disponíveis para a transformação completa do seu sorriso. A REOP Odontologia e Estética trabalha com todo cuidado e atenção para corrigir imperfeições com versatilidade e compromisso. Agende sua consulta para acompanhamento completo para instalação de sua <strong>lente de porcelana </strong>em SP conosco!</p>
<p>Diferentemente das facetas, a <strong>lente de porcelana em SP</strong> não precisa de grande desgaste no dente original para ser fixada. Contando com lentes extremamente finas para serem aplicadas diretamente sobre o dente, sem dor ou incômodo grande, são totalmente versáteis, e hoje em dia são as maiores aliadas para correção em busca do sorriso perfeito, já que permitem correções sutis em questões anatômicas, dando cor mais agradável e natural para o sorriso do paciente.</p>
<p>Agende seu horário conosco e conheça mais sobre todo o procedimento de moldagem, fabricação e instalação da sua <strong>lente de porcelana em SP</strong> com a REOP Odontologia e Estética.</p>
<h2><strong>Colocar lente de porcelana em SP com a REOP Odontologia é doloroso?</strong></h2>
<p>O procedimento estético de <strong>lente de porcelana em SP</strong> com a REOP Odontologia é um procedimento indolor, não invasivo, com resultados estéticos excelentes. As lentes são feitas para serem colocadas por cima do dente sem qualquer ou com mínimo desgaste, dando um aspecto natural, anatomicamente bonito e com harmonia. Marque uma consulta com nossa clínica especializada em procedimentos estéticos odontológicos como a moldagem, confecção e instalação de facetas e lentes de contato dentárias.</p>
<h2><strong>Lente de porcelana em SP com alta qualidade e com preço bom é na REOP Odontologia</strong></h2>
<p>Conheça a nossa de Odontologia e Estética. Especializados em procedimentos estéticos em geral e tratamentos odontológicos para reabilitação oral, a clínica fica localizada na Avenida Paulista. Para você que procura reabilitar seu sorriso com harmonia e sem muitos procedimentos invasivos, marque sua consulta para avaliação profissional e tenha mais informações sobre o procedimento completo de colocação de <strong>lente de porcelana em SP</strong>.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>