<?php
    $title       = "Odontopediatria em SP";
    $description = "A odontopediatria em SP nada mais é que uma especialidade dentro da odontologia, voltada principalmente para cuidar da saúde bucal, desde bebês até adolescentes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A REOP é referência no segmento <strong>odontopediatria em SP</strong>, focada em fazer o melhor possível sempre, com conforto, praticidade e cordialidade; otimizar o tempo de atendimento, interpretando o desejo das pessoas com uma linguagem gentil e clara. Além disso, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece.</p>
<p>De forma sucinta, a <strong>odontopediatria em SP</strong> tem como objetivo o diagnóstico, a prevenção, o tratamento e o controle dos problemas de saúde bucal do bebê, da criança e do adolescente; a educação para a saúde bucal e a integração desses procedimentos com os dos outros profissionais da área da saúde. Mas não se preocupe, pois, a qualquer hora do dia estamos disponíveis para tirar todas as suas dúvidas.</p>
<h2><strong>O que é e para que serve a odontopediatria em SP?</strong></h2>
<p>A <strong>odontopediatria em SP</strong> nada mais é que uma especialidade dentro da odontologia, voltada principalmente para cuidar da saúde bucal, desde bebês até adolescentes.</p>
<p>O profissional da área é essencial para acompanhar todo o crescimento e a formação dentária das crianças, garantindo que o sorriso dos pequenos fique longe dos problemas. É muito comum durante a infância, a formação de cáries e a pessoa responsável por tratar isso é justamente o profissional da <strong>odontopediatria em SP</strong>. O melhor sempre é prevenir, por isso não deixe para escolher e conhecer o <strong>odontopediatria em SP</strong> que vai cuidar do sorriso da sua criança.</p>
<p>O processo de higienização também deve seguir cuidadosamente as orientações da <strong>odontopediatria em SP</strong>, pois, é muito importante que você busque um profissional qualificado.</p>
<p>Por isso, os pais devem ter consciência de que é uma parte muito importante da rotina, e que, por mais muitas que reclamem ou chorem, ela não deve ser deixada de lado. </p>
<h2><strong>A REOP e odontopediatria em SP:</strong></h2>
<p>Em um mesmo local especialidades diversas trabalham juntas compartilhando as necessidades de nossos pacientes com profissionais capacitados em várias áreas e com recursos tecnológicos apropriados; sempre considerando o desejo do paciente e sua satisfação ao final do trabalho.</p>
<p>Além disso, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua contratação. Vale salientar que, desde o início, é estabelecida uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos a risca. Deixe os detalhes conosco, ligue agora mesmo e faça um orçamento sem compromisso. Estamos esperando por você.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>