<?php
$title       = "Dentista Urgente no Jabaquara";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Atendimentos de Dentista Urgente no Jabaquara em São Paulo é com a nossa clínica odontológica REOP, um local que oferece serviços de odontologia completos. Temos atendimento personalizado, prestativo e com muita atenção, buscando sempre realizar seus procedimentos com gentileza e respeito. Serviços gerais para você com todo profissionalismo e tecnologia do mercado.</p>
<p>Como líder do segmento de DENTISTA, a REOP ODONTO se dispõe a oferecer uma excelente assessoria nas questões de Dentista Urgente, Consultório Odontológico que Faz Bichectomia, Periodontia especializada, Odontopediatria e Bruxismo sempre com muita qualidade e dedicação aos seus clientes e parceiros. Por contarmos com uma equipe profissionalmente competente para proporcionar o melhor em Dentista Urgente no Jabaquara ganhamos a confiança e preferência de nossos clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>