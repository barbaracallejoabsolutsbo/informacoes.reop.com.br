<?php
$title       = "Clareamento Odontológico na Vila Andrade";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existem vários tipos de tratamento de clareamento. Encontre Clareamento Odontológico na Vila Andrade a laser, monitorado ou híbrido com a REOP Odontologia e Estética. Cada tipo de tratamento tem seus prós e contras, mas garantem resultado estético e satisfação recuperando o branco natural dos seus dentes. Agende já uma consulta para uma análise técnica e tratamentos especiais para você.</p>
<p>Como líder do segmento de DENTISTA, a REOP ODONTO se dispõe a oferecer uma excelente assessoria nas questões de Harmonização Facial Especializada, Aparelho Dentário Valor, Consultório Odontológico que Faz Bichectomia, Prótese Dentária Preço e Emergência Odontológica sempre com muita qualidade e dedicação aos seus clientes e parceiros. Por contarmos com uma equipe profissionalmente competente para proporcionar o melhor em Clareamento Odontológico na Vila Andrade ganhamos a confiança e preferência de nossos clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>