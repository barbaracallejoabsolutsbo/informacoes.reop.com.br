<?php
$title       = "Lente de Contato Dental na Sé";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A resposta é não. Colocar a Lente de Contato Dental na Sé tem sido um dos procedimentos mais indolores e tranquilos do ramo estético odontológico. Isso porque as pequenas lentes são instaladas por cima dos dentes sem a necessidade de qualquer tipo de desgaste. Disponíveis em porcelana e resina, aqui você tem atendimento profissional e qualificado para procedimentos estéticos e funcionais odontológicos.</p>
<p>Você procura por Lente de Contato Dental na Sé? Contar com empresas especializadas no segmento de DENTISTA é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa REOP ODONTO é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Tratamento de Endodontia, Clareamento Odontológico, Implante de Dentes de Resina, Extração de dente do siso e Aparelho Odontológico e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>