<?php
    $title       = "Tratamento de Endodontia";
    $description = "A REOP possui uma vasta experiência no ramo de tratamento de endodontia e está sempre atenta as atualizações do mercado para superar as suas expectativas. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>De forma sucinta, o <strong>tratamento de endodontia </strong>é uma especialidade destinada ao diagnóstico, tratamento e prevenção das doenças que atingem a parte interna do dente: a polpa, a raiz e os tecidos periapicais, que envolvem a raiz. É uma das áreas da odontologia fundamentais para a saúde bucal e tratamentos relacionados a reabilitação oral.</p>
<p>Por sua vez, a REOP possui uma vasta experiência no ramo de <strong>tratamento de endodontia</strong> e está sempre atenta as atualizações do mercado para superar as suas expectativas. Em nossa primeira consulta, o profissional procura conhecer as necessidades e desejos do paciente, garantindo um material específico para apresentar um diagnóstico e um plano de tratamento.</p>
<h2><strong>Entendendo o </strong><strong>tratamento de endodontia:</strong></h2>
<p>O profissional responsável pelo <strong>tratamento de endodontia</strong> atua na correção de trincas, fraturas e outros traumas que venham a comprometer a estrutura interior do dente. A intervenção do endodontista é necessária para prevenir a ocorrência de infecções graves e para evitar a necrose completa, levando à perda dentária.</p>
<p>No <strong>tratamento de endodontia</strong> é realizado um diagnóstico, a terapêutica e a profilaxia das doenças que atingem a parte interna do dente (polpa), da raiz e dos tecidos em volta dele (tecido periapical).</p>
<p>O <strong>tratamento de endodontia</strong> é indicado para pacientes nos quais a cárie chega até a polpa do dente, que acaba comprometendo a raiz. Sendo assim, a necessidade de tratar o canal deve ser avaliada nos seguintes casos:</p>
<ul>
<li>         cáries profundas;</li>
<li>         dentes quebrados ou trincados;</li>
<li>         sensibilidade intensa, especialmente a alimentos frios e quentes, entre outros.</li>
</ul>
<p>Ao realizar o <strong>tratamento de endodontia</strong>, entre outras complicações, o paciente pode evitar a ocorrência de quadros sistêmicos de infecção, que surgem quando as bactérias presentes na cavidade bucal chegam à corrente sanguínea.</p>
<h2><strong>Por que realizar o </strong><strong>tratamento de endodontia com a REOP?</strong></h2>
<p>Atualmente, nós somos referência no segmento odontológico, focados em fazer o melhor possível sempre, com conforto, praticidade e cordialidade; otimizar o tempo de atendimento, interpretando o desejo das pessoas com uma linguagem gentil e clara. Além disso, desde o início, é estabelecida uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos a risca.</p>
<p>Respeitar o nosso ambiente de trabalho e as pessoas com as quais atuamos é imprescindível e estarmos unidos pelo bem comum prestando ótimo serviço com o reconhecimento de nossos pacientes, colegas e parceiros. Se interessou? Ligue agora mesmo e faça um orçamento sem compromisso com a nossa equipe. Esperamos por você.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>