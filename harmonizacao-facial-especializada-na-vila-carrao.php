<?php
$title       = "Harmonização Facial Especializada na Vila Carrão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A harmonização facial é um procedimento que está em alta em clínicas estéticas e no mundo das celebridades. Isso porque se trata de um procedimento que corrige possíveis falhas e traços que atrapalham a harmonia do rosto, dando um aspecto de beleza natural e saudável quando realizado de forma correta. Na REOP Odontologia e Estética você encontra Harmonização Facial Especializada na Vila Carrão, segura e com resultados surpreendentes.</p>
<p>A empresa REOP ODONTO é destaque entre as principais empresas do ramo de DENTISTA, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Harmonização Facial Especializada na Vila Carrão do mercado. Ainda, possui facilidade com Aparelho Dentário Valor, Lente de Contato de Porcelana, Implante Odontológico, Invisalign preço e Aparelho ortodôntico invisível mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>