<?php
    $title       = "Clínica Odontológica Especializada";
    $description = "Todas as nossas informações para contato estão em nosso site, entre para que mantenhamos contato. Whatsapp: (11) 96325-1772 ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Precisando de Clínica Odontológica Especializada, esperamos que nossa clínica seja o primeiro lugar no qual você sabe que te dará o retorno que você precisa. Estamos há mais de 18 anos buscando cada vez mais aperfeiçoar nosso profissionalismo para que possamos fazer serviços odontológicos qualificados para nossos clientes. Em nossa Clínica Odontológica Especializada, você encontra equipamentos de primeira mão e salas de atendimento individuais para que cada cliente tenha a total atenção que merece, mantendo sempre a ética e empatia dos nossos profissionais.  </p>
<p> <br /> Ao procurar por Clínica Odontológica especializada, você encontrará diversos serviços que uma só clínica pode fazer. O que não é diferente na Clínica Roep; para cada necessidade do cliente, possuímos derivados equipamentos e diversos profissionais de diferentes especialidades. Priorizamos sempre em nossa clínica, que o seu objetivo em qualquer e todo tratamento seja concluído. Em todo o processo mantemos contato com você para saber a sua satisfação com nosso serviço e enviamos e-mail lembrete de checkup para que ele ocorra da forma correta e sem atraso. Quando notar qualquer tipo de divergência em sua região bucal, não deixe de procurar nossa Clínica Odontológica Especializada, pois é de extrema importância mantermos nossa saúde dentária, para que possamos fazer coisas simples do dia a dia, como comer, falar... Fazemos a questão de que assim que nos consultar, você tenha cada vez menos problemas com seus dentes. </p>
<p>Nossa Clínica Odontológica especializada atua nesse mercado desde 2001 e ao longo desses anos, conquistamos cada vez mais pacientes, pois nosso objetivo é sempre priorizar as vontades de cada um, nos adaptando a eles, mantendo sempre o bom humor e a boa recepção pois nosso maior prazer é ver nossos pacientes extremamente satisfeitos com nosso trabalho. Fazemos questão em te ajudar a resolver o seu problema, pois o seu bem estar é o que nos importa. </p>
<h2>Encontre Clínica Odontológica Especializada  </h2>
<p>Somente uma clínica de qualidade pode oferecer diversos serviços em uma única unidade. Não deixe de nos consultar para fazer seu orçamento desejado. Não exite em nos procurar pois nosso maior prazer é te atender. </p>
<h2>A melhor opção para Clínica Odontológica Especializada  </h2>
<p>Esperamos que seu primeiro contato com um dos nossos profissionais em nossa Clinica de Ortodontia Especializada, faça com que você se orgulhe de ter nos encontrado, pois é de extremo agrado vermos sua evolução dentária e fazemos o possível para efetuarmos isso juntos. Consulte-nos através das nossas redes sociais, estamos sempre à disposição. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>