<?php
$title       = "Placa de bruxismo no Bairro do Limão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está procurando por placas para tratamento de bruxismo, a REOP Odontologia pode te auxiliar. A Placa de bruxismo no Bairro do Limão, também conhecida como placa de mordida, é um dispositivo com o formato da sua arcada dentária que se ajusta perfeitamente sobre os seus dentes. Feito para ser utilizado durante o tempo que for preciso durante o dia a fim de evitar o desgaste e desalinhamento dos dentes pela tensão na mandíbula e arcada dentária.</p>
<p>Nós da REOP ODONTO trabalhamos dia a dia para garantir o melhor em Placa de bruxismo no Bairro do Limão e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de DENTISTA com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Implantes de Dentes de Porcelana, Clínica Odontológica Especializada, Odontopediatria Preço, Prótese Dentária Preço e Lente de Porcelana e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>