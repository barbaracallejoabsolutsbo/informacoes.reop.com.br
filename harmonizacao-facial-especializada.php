<?php
    $title       = "Harmonização Facial Especializada";
    $description = "Encontre procedimentos estéticos, harmonização facial especializada, bichectomia, implantes dentários, reabilitação oral completa e muito mais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Encontre procedimentos estéticos, <strong>harmonização facial especializada</strong>, bichectomia, implantes dentários, reabilitação oral completa e muito mais. A REOP Odontologia e Estética oferece serviços e atendimentos com profissionais especializados e experientes a fim de proporcionar resultados ímpares e altamente satisfatórios. Trabalhamos com a auto estima e bem estar comum, reabilitando sorrisos e rejuvenescendo faces a partir de procedimentos estéticos, odontológicos e ortodônticos realizados em nossa clínica em SP.</p>
<p>A harmonização facial é um pequeno procedimento popular em clínicas de estética. É perfeito porque é um procedimento para corrigir defeitos e características que podem interferir na harmonia facial e, se executado de maneira adequada, proporcionará às pessoas uma beleza natural e saudável aos traços faciais. Na REOP Odontologia e Estética você encontrará <strong>harmonização facial especializada</strong>, segura e com resultados surpreendentes.</p>
<p>Para realizar de forma segura o procedimento de harmonização facial com a REOP, é necessário agendar uma consulta para a avaliação de um de nossos profissionais, a fim de avaliar os pontos a serem trabalhados para orçamento e cotação. Além disso, é possível encontrar diversos outros provimentos estéticos para renovar o visual da sua face e renovar também o seu sorriso. Conheça mais sobre a <strong>harmonização facial especializada </strong>e outros procedimentos estéticosrealizados na REOP Odontologia e Estética.</p>
<p>A REOP possui centro estético que oferece diversos procedimentos que promovem o rejuvenescimento da pele, ameniza rugas e manchas, trata, limpa e hidrata a pele proporcionando resultados incríveis. Fale conosco para orçamentos e cotações de serviços como nossa <strong>harmonização facial especializada</strong>.</p>
<h2><strong>Agende já sua harmonização facial especializada com a REOP e renove seu visual</strong></h2>
<p>Fale com nossos atendentes e agende já sua consulta para avaliação profissional e preparo para sua<strong> harmonização facial especializada </strong>com a REOP Clínica de Odontologia e Estética. Há mais de 20 anos no mercado, com profissionais experientes, prezamos por resultados satisfatórios e serviços executados com excelência.</p>
<h2><strong>Reabilitação oral, harmonização facial especializada, bichectomia e muito mais</strong></h2>
<p>A REOP Odontologia e Estética trabalha com objetivo principal de resgatar e melhorar ainda mais a sua autoestima. Com serviços especializados de clínica estética e clínica odontológica, trabalhamos desde a reabilitação oral completa até procedimentos estéticos como<strong> harmonização facial especializada</strong>, bichectomia, peeling, massagens e limpezas de pele. Consulte nosso catálogo e fale conosco para mais informações.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>