<?php
    $title       = "Lente de resina em SP";
    $description = "Atualmente, nós fazemos parte do cotidiano do público que atendemos, sendo reconhecidos como uma clínica de referência na área de lente de resina em SP.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está procurando por <strong>lente de resina em SP</strong> de qualidade, você está no lugar certo. A Reop é uma clínica odontológica que possui uma vasta experiência neste ramo e conta com profissionais qualificados para não somente atender a sua necessidade, mas também, superar todas as suas expectativas.</p>
<p>Em geral, a nossa missão é estimular e promover qualidade de vida cuidando e otimizando o dia a dia das pessoas. Por isso, estamos sempre atentos as atualizações do mercado para fornecer a <strong>lente de resina em SP</strong> mais moderna e eficiente ao cliente.</p>
<h2><strong>Como funciona a lente de resina em SP?</strong></h2>
<p>Primeiramente, a <strong>lente de resina em SP</strong> nada mais é que  uma cópia idêntica de um dente natural realizada através da adição de resina composta, no qual a resina recobre a parte da frente do dente como uma capa.</p>
<p>Assim, a <strong>lente de resina em SP</strong> mascara imperfeições que mereçam tratamento, seja uma mancha, fratura, trinca ou aparência envelhecida do dente. Lembrando que, a faceta pode ou não precisar de desgaste da superfície do dente, somente o dentista poderá avaliar cada situação.</p>
<p>Ou seja, se você deseja ter um belo sorriso, de forma rápida e prática, a <strong>lente de resina em SP</strong> é uma excelente opção, pois o que lhe incomoda e tira a beleza de seu sorriso não será mais um incômodo com esta solução. Por isso, procure por uma empresa especializada para realizar tal serviço.</p>
<h2><strong>Vantagens da lente de resina em SP da REOP:</strong></h2>
<p>Conosco, você tem a tranqüilidade e segurança de escolher uma <strong>lente de resina em SP</strong> de qualidade, através de profissionais qualificados. Além disso, pensando em seu bem estar completo, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para deixar o seu sorriso ainda mais bonito.</p>
<p>Atualmente, somos referência no segmento odontológico, focados em fazer o melhor possível sempre, com conforto, praticidade e cordialidade; otimizar o tempo de atendimento, interpretando o desejo das pessoas com uma linguagem gentil e clara.</p>
<p>Vale frisar ainda que, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos a risca. No momento em que entrar em contato com a nossa equipe, você notará que encontrou a empresa ideal para se tornar um novo parceiro de longa data. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Ligue agora mesmo e faça um orçamento totalmente sem compromisso.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>