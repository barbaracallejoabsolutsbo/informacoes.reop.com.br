<?php
$title       = "Prótese Protocolo Superior Preço em Itaim Bibi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Prótese Protocolo Superior Preço em Itaim Bibi é uma das soluções mais modernas e definitivas para solução de reabilitação oral de pacientes que acabaram por perder todos ou quase todos os dentes. Normalmente funciona como prótese fixa composta principalmente por 12 dentes presos em uma estrutura de resina, parafusados em 4, 5 ou 6 parafusos, dando total conforto e dispensando as dentaduras ou próteses removíveis.</p>
<p>Entre em contato com a REOP ODONTO se você busca por Prótese Protocolo Superior Preço em Itaim Bibi. Somos uma empresa especializada com foco em Aparelho Dental Transparente, Aparelho Dentário Valor, Lente de Porcelana, Lente de Contato Dental e Aparelho Dental onde garantimos o melhor para nossos clientes, uma vez que, contamos com o conhecimento adequado para o ramo de DENTISTA. Entre em contato e faça um orçamento com um de nossos especialistas e garanta o melhor custo x benefício do mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>