<?php
    $title       = "Periodontia em SP";
    $description = "São diversos os tratamentos que englobam a periodontia em SP que vão desde a prevenção da doença gengival até enxertos gengivais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Na REOP, o cliente encontra a melhor <strong>periodontia em SP, </strong>pois, somos um local que oferece uma odontologia completa, tendo o foco no atendimento personalizado, prestativo, atencioso, interpretando o desejo das pessoas com uma linguagem gentil, clara e competente. Além disso, contamos com uma equipe unida e organizada que está sempre atenta as modernidades para fornecer serviços de última geração.</p>
<p>A <strong>periodontia em SP </strong>nada mais é que uma especialidade da odontologia direcionada para a prevenção, diagnóstico e tratamento de condições que acometem os tecidos de sustentação dos dentes, ou seja, as gengivas, ligamento periodontal e ossos alveolares. </p>
<h2><strong>Conhecendo melhor a periodontia em SP:</strong></h2>
<p>Em suma, o dentista da <strong>periodontia em SP</strong> não realiza seu trabalho diretamente no dente, mas nas estruturas que estão ao seu redor, prevenindo problemas que podem afetar o elemento dental que está sadio, sendo uma especialidade fundamental para manter a boca saudável.</p>
<p>A <strong>periodontia em SP</strong> pode ser utilizada em 3 situações. Confira abaixo:</p>
<p>Periodontia clínica: É direcionada para a prevenção de problemas bucais, por meio de procedimentos como a profilaxia dentária profissional, e para o tratamento de doenças periodontais, como a gengivite e periodontite.</p>
<p>Periodontia médica: É uma atuação relacionada às condições sistêmicas presentes ou agravadas pela presença da doença periodontal, como AVC, por exemplo.</p>
<p>Periodontia estética: têm como finalidade melhorar a estética periodontal do paciente, no qual são chamadas cirurgias plásticas periodontais, como a de sorriso gengival e de recobrimento radicular.</p>
<p>A fim de evitar problemas graves, a visita ao periodontista deve ser feita a cada seis meses para prevenção e manutenção da saúde bucal. Nas consultas de check-up com esse especialista, devem ser realizadas uma avaliação bucal geral, uma limpeza completa.</p>
<h2><strong>Pensou em periodontia em SP, pensou na REOP!</strong></h2>
<p>São diversos os tratamentos que englobam a <strong>periodontia em SP</strong> que vão desde a prevenção da doença gengival até enxertos gengivais. Atualmente, os tratamentos mais procurados na clínica: sangramento gengival, mau hálito, doença periodontal, retração gengival, plástica gengival e raspagem.</p>
<p>Vale salientar que, pensando no bem estar completo de nossos clientes, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua contratação. No momento em que entrar em contato conosco, você notará que encontrou a empresa ideal para se tornar o mais novo parceiro de longa data. Venha saber mais sobre a melhor <strong>periodontia em SP</strong>. Esperamos por você.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>