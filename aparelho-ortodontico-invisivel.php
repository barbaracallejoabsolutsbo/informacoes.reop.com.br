<?php
    $title       = "Aparelho ortodôntico invisível";
    $description = "Para saber mais sobre o aparelho ortodôntico invisível ou quaisquer outros procedimentos odontológicos ou estéticos realizados pela REOP consulte os artigos em nosso site.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura por uma opção mais discreta, apresentamos o <strong>aparelho ortodôntico invisível. </strong>Nosso <strong>aparelho ortodôntico invisível </strong>é muito discreto e por muitas vezes imperceptível por ser transparente. Essa opção é uma ótima escolha para pessoas que tenham receio quanto a estética dos aparelhos ortodônticos. A REOP é uma clínica odontológica e estética dentre as mais recomendadas do Brasil. Nossos profissionais se preocupam em acompanhar o desenvolvimento do mercado dentro do nosso segmento para sempre trazer inovações que proporcionem melhores resultados com condições mais agradáveis para nossos clientes. O <strong>aparelho ortodôntico invisível </strong>é um exemplo disso por ser um recurso moderno e recente que já é realizado com maestria em nossa clínica. Com localização privilegiada na Avenida Paulista, um dos centros comerciais mais movimentados do mundo, nossa clínica é de fácil acesso tanto para transporte público como para transporte privado. São inúmeros os benefícios que nos colocam passos a frente quando nossos clientes devem escolher uma clínica odontológica.</p>
<p>O sorriso é uma das principais primeiras impressões que temos é uma pessoa ao conhecê-la, por isso diversas pessoas podem sofrer até mesmo problemas de autoestima por conta da estética dentária. Volte a sorrir sem medo com a correção de nosso <strong>aparelho ortodôntico invisível. </strong>Nossa clínica oferece diversas vantagens como 5 salas de atendimento individualizadas, exame dentário com câmera intra-oral, e-mail de lembrete de consulta, parcelamento e tratamentos com facilidade.  Não perca tempo e realize sua avaliação para colocar <strong>aparelho ortodôntico invisível </strong>em uma das melhores clínicas de São Paulo. Com a visão de fazer parte do cotidiano do público que atendemos e sermos reconhecidos como uma clínica referência em nosso segmento, trabalhamos com muita seriedade e compromisso. Nossa empresa se preocupa em atualizar constantemente seus recursos para que nossos clientes tenham acesso aos melhores métodos de tratamento atualmente.</p>
<h2><strong>Saiba mais sobre o aparelho ortodôntico invisível.</strong></h2>
<p>Para saber mais sobre o <strong>aparelho ortodôntico invisível</strong> ou quaisquer outros procedimentos odontológicos ou estéticos realizados pela REOP consulte os artigos em nosso blog ou entre em contato e seja atendido por um especialista para tirar suas dúvidas.</p>
<h2><strong>O melhor lugar para colocar aparelho ortodôntico invisível em São Paulo.</strong></h2>
<p>Consulte as avaliações e aumente ainda mais sua confiabilidade em colocar seu <strong>aparelho ortodôntico invisível</strong> conosco. Somos referência nacional em odontologia e estética para cuidar da sua saúde bucal da melhor maneira possível.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>