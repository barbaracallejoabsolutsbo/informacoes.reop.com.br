<?php
    $title       = "Periodontia especializada em SP";
    $description = "Quem conhece o serviço de periodontia especializada em SP da REOP, pode confirmar a excelência desde o atendimento personalizado que é oferecido, até o serviço completo. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Quem conhece o serviço de <strong>periodontia especializada em SP </strong>da REOP, pode confirmar a excelência desde o atendimento personalizado que é oferecido, até o serviço completo. Não hesitamos em fazer um bom trabalho e contamos com profissionais qualificados para oferecer as novas técnicas e possibilidades de tratamentos.</p>
<p>É importante frisar que, atuamos em várias áreas da odontologia integradas para viabilizar os melhores resultados possíveis. Por isso, no momento em que entrar em contato conosco, você notará que encontrou a <strong>periodontia especializada em SP</strong> ideal a sua necessidade.</p>
<h2><strong>Como escolher a periodontia especializada em SP certa?</strong></h2>
<p>O indicado é sempre o paciente avaliar a formação do dentista. Isso porque é comum encontrar profissionais de odontologia clínica que atuam como especialistas em periodontia.</p>
<p>A <strong>periodontia especializada em SP</strong> é a especialidade que trata desses tecidos e também atua na prevenção de problemas nessas estruturas, garantindo, assim, um sorriso saudável. Para receber o melhor atendimento nessa área, busque uma clínica completa e conceituada no mercado.</p>
<p>Em geral, para receber um atendimento de qualidade e os melhores tratamentos na área de <strong>periodontia especializada em SP</strong>, é necessário buscar clínicas de referência.</p>
<p>Vale lembrar que, o dentista responsável pela <strong>periodontia especializada em SP</strong> não realiza seu trabalho diretamente no dente, mas nas estruturas que estão ao seu redor, prevenindo problemas que podem afetar o elemento dental que está sadio. É, portanto, uma especialidade fundamental para manter a boca saudável.</p>
<p>Em termos mais simples, podemos dizer que a <strong>periodontia especializada em SP </strong>consiste na estrutura de suporte e sustenção que envolve o dente (gengiva, osso e ligamento periodontal). A gengiva reveste e protege e dá suporte ao osso; o ligamento periodontal são fibras que unem o dente ao osso; por fim o osso é a estrutura que dá sustentação ao dente. </p>
<h2><strong>Periodontia especializada em SP de qualidade é na REOP!</strong></h2>
<p>Primeiramente, é feito um estudo por nossos profissionais sobre às causas da fase da periodontite. Os casos mais simples são resolvidos, em sua maioria, com a limpeza, remoção do calculo dental e orientação de higiene. Já os casos mais complexos são feitos com cirurgia e associação de medicamentos. Porém, o osso perdido não retorna ao estado inicial, por isso a prevenção e as consultas periódicas são muito importantes.</p>
<p>Mas não se preocupe, pois, pensando em você, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua contratação. Está esperando o que para ligar agora mesmo e tirar todas as suas dúvidas? Esperamos por seu contato a qualquer hora do dia.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>