<?php
    $title       = "Invisalign em SP";
    $description = "O Invisalign em SP é um tipo de alinhador dentário diferente dos comuns. Sua estrutura é totalmente dos convencionais aparelhos ortodônticos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O <strong>Invisalign em SP</strong> é um tipo de alinhador dentário diferente dos comuns. Sua estrutura é totalmente dos convencionais aparelhos ortodônticos. É basicamente uma placa discreta, que lembra placas de clareamento dentário ou para tratamentos de ATM. Para o tratamento completo, é utilizado um conjunto de placas que devem ser trocadas e confeccionadas de tempos em tempos, para movimentação de conjuntos de dentes diferentes entre as trocas.</p>
<p>Com a mesma função dos alinhadores ortodônticos tradicionais com braquetes, o Invisalign pode alinhar os seus dentes sem fios metálicos, consistindo em um jogo de moldeiras removíveis e praticamente invisíveis que são utilizadas durante o dia a dia de forma instruída pelo dentista. Com a REOP Odontologia e Estética você encontra todo suporte para tratamentos com <strong>Invisalign em SP</strong>.</p>
<p>O tratamento com <strong>Invisalign em SP</strong> com nossa clínica pode durar entre 12 a 18 meses, conseguindo ver os primeiros resultados já nas primeiras semanas. Com maior agilidade, conforto e estética, o Invisalign é uma alternativa que garante alinhar seus dentes sem processos invasivos e demorados. Disponível na REOP você encontra diversos tratamentos odontológicos, ortodônticos e estéticos com preço acessível e profissionalismo.</p>
<p>Dentes desalinhados podem prejudicar a estética do seu sorriso e atrapalhar sua mastigação, dicção e até mesmo causando cáries e problemas periodontais. Para evitar todos estes problemas, alinhar os dentes é sempre uma ótima opção. Entre os tratamentos especializados para alinhamento de dentes, a REOP oferece todo suporte <strong>Invisalign em SP</strong>, do início ao fim, com acompanhamento periódico para troca das placas alinhadoras e cuidados na saúde bucal. Fale conosco e agende já sua consulta com a REOP Odontologia e Estética.</p>
<h2><strong>A tecnologia Invisalign em SP, conheça a REOP Odontologia e seus tratamentos já!</strong></h2>
<p>O <strong>Invisalign em SP</strong> é uma tecnologia do mercado odontológico que se trata de um alinhador ortodôntico transparente, inovador, em formato de placas que lembram placas para bruxismo ou clareamento dental, porém em material diferente e mais firme, e com funções diferentes. É um tratamento alternativo e discreto, além de ser removível, não precisando fixar fios e braquetes. O tratamento se baseia em conjuntos de placas removíveis invisíveis, devendo ser usadas durante o tratamento de acordo com a indicação do dentista responsável.</p>
<h2><strong>O Invisalign em SP funciona realmente?</strong></h2>
<p>Com custo superior ao tratamento convencional, esse tratamento é bem cômodo e discreto, proporcionando resultados surpreendentes e extremamente ágeis se comparados com tratamentos populares. Com resultados nos primeiros meses, o tratamento dura de 12 meses a 1 ano e meio. Encontre nosso tratamento completo com aparelho ortodôntico <strong>Invisalign em SP</strong> com a REOP Odontologia.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>