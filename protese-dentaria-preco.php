<?php
    $title       = "Prótese Dentária Preço";
    $description = "Consultar um profissional a cada 6 meses é primordial para evitar consequências desagradáveis e também, a quebra de prótese dentária preço.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Bem vindo a REOP, clínica de qualidade que oferece a <strong>prótese dentária preço</strong> adequado.  Entre as nossas diversas ideologias, vale salientar que, buscamos a excelência com humildade durante todo o contato com o paciente, explicando suas necessidades de uma forma didática, simples e clara.</p>
<p>Desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados a <strong>prótese dentária preço</strong> sejam cumpridos a risca. Além disso, a qualquer hora do dia, estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção.</p>
<h2><strong>Conhecendo melhor a prótese dentária preço</strong></h2>
<p>Primeiramente, existem diferentes tipos de <strong>prótese dentária preço</strong>, e por isso é fundamental passar por um processo de avaliação com um especialista antes. Dessa forma, o dentista poderá encaminhá-lo para a melhor forma de tratamento, e com as devidas orientações.</p>
<p>A avaliação inicial é necessária porque cada tipo de <strong>prótese dentária preço</strong> busca reparar problemas específicos. Vamos conhecer alguns modelos disponíveis:</p>
<ul>
<li>         Prótese total removível: É comumente relacionada a pacientes da terceira idade, que costumam estar com a saúde bucal mais fragilizada nessa fase da vida. </li>
<li>         Prótese parcial removível: É utilizada quando o indivíduo possui alguns dentes. Isto é, quando o paciente não sofreu a perda de todos os dentes e também não é necessário extrair os elementos dentais remanescentes para a colocação da prótese.</li>
<li>         Prótese parcial fixa: Esse tipo de prótese pode ser de dois tipos: as coroas ou as pontes, onde cada uma é recomendada para casos diferentes por terem suas especificidades.</li>
</ul>
<p>Consultar um profissional a cada 6 meses é primordial para evitar consequências desagradáveis e também, a quebra de <strong>prótese dentária preço</strong>. Além disso, o profissional também pode observar o estado da prótese, verificando se há ou não a necessidade de realizar ajustes na peça.</p>
<h2><strong>A </strong><strong>prótese dentária preço ideal</strong></h2>
<p>A REOP é uma empresa odontológica cuja fundadora atua na área a 20 anos, acreditando que para alcançar uma prestação de serviço com excelência, completa e com melhores resultados é necessário unir conhecimento, assim o fizemos.</p>
<p>Em um mesmo local especialidades diversas trabalham juntas compartilhando as necessidades de nossos pacientes com profissionais capacitados em várias áreas e com recursos tecnológicos apropriados; sempre considerando o desejo do paciente e sua satisfação ao final do trabalho. Além disso, agregamos o melhor custo benefício do ramo de <strong>prótese dentária preço</strong> com diversas formas de pagamento. Venha conferir agora mesmo.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>