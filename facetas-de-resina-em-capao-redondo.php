<?php
$title       = "Facetas de Resina em Capão Redondo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As Facetas de Resina em Capão Redondo são restaurações ou placas de resina que são cimentadas aplicadas em cima da superfície dos dentes com formatos anatomicamente harmoniosos e confortáveis. Tem como objetivo principal corrigir variações de cor e de formato dos dentes. As Facetas de Resina em Capão Redondo têm espessura maior que as lentes de contato, e precisam de pequeno desgaste para encaixe perfeito e confortável.</p>
<p>A REOP ODONTO, além de ser especializados em Prótese Protocolo Superior Preço, Harmonização do Rosto Valor, Odontopediatria, Prótese Dentária Preço e Clareamento Odontológico, atuando no mercado de DENTISTA com a missão de oferecer sempre o melhor para seus clientes, de forma que seus objetivos sejam alcançados. Além disso, contamos com profissionais competentes visando prestar o melhor atendimento possível em Facetas de Resina em Capão Redondo para ser sempre a opção número um de nossos parceiros e clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>