<?php
$title       = "Bruxismo em Morro Grande - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Bruxismo em Morro Grande - Guarulhos nada mais é do que o hábito de tensionar e ranger os dentes. Muito mais presentes na vida adulta, mas não deixando de atingir também as crianças, está muito relacionado ao stress, nervosismo ou simplesmente ao mau hábito de tensionar os dentes intencionalmente por costume. Essa é uma prática muito prejudicial para os dentes, mas tem tratamento com placas e terapias. Encontre suporte total com a REOP.</p>
<p>Se deseja comprar Bruxismo em Morro Grande - Guarulhos e procura por uma empresa séria e competente, a REOP ODONTO é a melhor opção. Com uma equipe formada por profissionais experientes e qualificados, dos quais trabalham para oferecer soluções diferenciadas para o projeto de cada cliente. Clínica de Ortodontia, Clínica Odontológica Especializada, Clareamento Dental Valor, Prótese Dentária Preço e Periodontia especializada. Entre em contato e saiba tudo sobre DENTISTA com os melhores profissionais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>