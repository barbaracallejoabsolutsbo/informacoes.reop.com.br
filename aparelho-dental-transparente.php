<?php
    $title       = "Aparelho Dental Transparente";
    $description = "O aparelho dental transparente é uma ótima opção para você que gostaria de realizar um tratamento com um aparelho mais discreto e esteticamente mais agradável para o seu gosto.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O <strong>aparelho dental transparente </strong>é uma ótima opção para você que gostaria de realizar um tratamento com um aparelho mais discreto e esteticamente mais agradável para o seu gosto. Os aparelhos dentais são muito importantes para a saúde bucal dos pacientes tendo em vista as diversas correções que são realizadas no alinhamento dos dentes proporcionando mais qualidade de vida para nossos pacientes. O alinhamento dos dentes pode trazer diversos benefícios para a saúde, prevenindo inclusive problemas gástricos proveniente de uma mastigação errada por conta do posicionamento dos dentes. Pode também auxiliar a diminuir dores de cabeça e principalmente problemas de oclusão (dificuldade de abrir e fechar a boca). O <strong>aparelho dental transparente </strong>é disponibilizado em diferentes versões que são utilizadas e indicadas de acordo com as necessidades da situação individual de cada caso. Agende agora mesmo sua avaliação conosco e tenha um plano de tratamento odontológico e estético de acordo com as características que você gostaria de mudar ou as necessidades de saúde.</p>
<p>A REOP é uma empresa conceituada do ramo de odontologia em São Paulo. Localizados na avenida paulista, um dos maiores centros comerciais da cidade, disponibilizamos duas salas para receber nossos clientes com conforto em uma grande estrutura. São 5 salas individuais que realizam atendimento simultâneo para que você não fique esperando. O <strong>aparelho dental transparente </strong>muitas vezes pode até passar despercebido por ser tão discreto, sendo assim mesmo para as pessoas que tenham medo de não lhe agradar esteticamente é possível oferecer um tratamento completo e resolutivo. Além de <strong>aparelho dental transparente </strong>realizamos diversos atendimento dos mais diferentes segmentos da odontologia e estética. Consulte nosso catálogo de serviços para descobrir tudo que a REOP pode oferecer para você.</p>
<h2><strong>Encontre aparelho dental transparente com os melhores custos de São Paulo.</strong></h2>
<p>Cuide do seu sorriso com o nosso <strong>aparelho dental transparente </strong>com preço acessível. Volte a mastigar e sorrir sem medo. São diversos benefícios para a estética e saúde do ser humano. Agende sua avaliação e conheça o plano de tratamento ideal de acordo com as suas necessidades. Realize o exame com câmera intra-oral para uma melhor avaliação.</p>
<h2><strong>Saiba mais sobre o aparelho dental transparente.</strong></h2>
<p>Para eventuais dúvidas sobre o <strong>aparelho dental transparente </strong>ou quaisquer outros procedimentos realizados pela REOP consulte nosso blog ou entre em contato para ser atendido por nossa equipe que irá explicar com paciência e didaticamente o passo a passo de cada procedimento.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>