<?php
$title       = "Odontopediatria Preço no Ipiranga";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você procura por Odontopediatria Preço no Ipiranga justo e com especialistas educados, pacientes e profissionais, acaba de encontrar o melhor local. A REOP Odontologia oferece diversos serviços para você e sua família para todo o ramo da odontologia e da estética também. Aqui você tem especialistas para todas faixas etárias para restaurações, alinhamento dentário, implantes e muito mais.</p>
<p>Líder no segmento de DENTISTA, a REOP ODONTO dispõe dos melhores e mais modernos recursos do mercado. Nossa empresa trabalha com o objetivo de viabilizar Odontopediatria Preço no Ipiranga com a qualidade que você tanto procura. Somos, também, especializados em Placa de bruxismo, Clínica Odontológica Especializada, Harmonização Facial Especializada, Tratamento de Endodontia preço e Implantes de Dentes de Porcelana, pois, contamos com uma equipe competente e comprometida em prestar um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>