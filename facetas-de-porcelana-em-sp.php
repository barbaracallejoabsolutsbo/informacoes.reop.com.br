<?php
    $title       = "Facetas de Porcelana em SP";
    $description = "Nosso Whatsapp para contato é o:  (11) 96325-1772. Se quiser nos fazer uma visita, consulte nosso endereço em nosso site. Estamos sempre à sua disposição";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p> Ter um sorriso bonito é algo que é desejado por todos nós. Aqui na clínica Reop  fazemos Facetas de Porcelana em SP com preços acessíveis para que esse objetivo não seja difícil de ser alcançado. Atuamos nesse ramo há mais de 15 anos e ao decorrer deles, aprendemos a cada vez mais satisfazer nossos pacientes, inovando sempre. Facetas de Porcelana em SP de qualidade não são fáceis de encontrar, por isso em nossa clínica usamos dos melhores materiais para que você possa ter o seu resultado da maneira que imaginou, ou até mesmo melhor.  </p>
<p>    Fazemos o acompanhamento antes, durante e após a colocação das facetas, para que suas necessidades sempre sejam atendidas. Mantemos contato pelo Whatsapp, mas mandamos e-mails lembrete de checkup se preferir. Ao longo dos diversos anos de experiência, nos tornamos a melhor clínica de colocação de Facetas de Porcelana em SP, pois possuímos profissionais que exercem essa especialidade há anos, tornando-se cada vez mais experientes nessa e em outras funcionalidades. Não exite em pensar na clínica Reop como uma opção para colocar Facetas de Porcelana em SP. Estamos sempre dispostos a atender com prontidão cada paciente que nos são designados, pois com todas as nossas experiências, aprendemos com cada um que já passou pela nossa clínica. Queremos sempre ajudar na autoestima de nossos pacientes e certificamos que o procedimento de facetas de porcelana é uma das melhores opções para que se obtenha um resultado mais rápido sem muita artificialidade, pois com nossos materiais renomados garantimos a naturalidade em seus dentes.  </p>
<p>      Temos estrutura qualificada para todos os tipos de serviços odontológicos, consequentemente a estrutura mais que necessária para aqueles que procuram fazer Facetas de Porcelana em SP. Nossa equipe está sempre disponível para você tirar suas dúvidas e caso queira até nos fazer uma visita, estamos prontos para lhe receber em nossa clínica. É um prazer atender suas necessidades com um de nossos serviços, estamos sempre à disposição.  </p>
<h2>     Mais detalhes sobre Facetas de Porcelana em SP  </h2>
<p>Consulte o melhor lugar para fazer Facetas de Porcelana em SP, na clínica Reop priorizamos sempre as vontades de nossos pacientes e fazemos de tudo para que elas sejam correspondidas. Possuímos profissionais que amam o que fazem e por isso fazem seus serviços A melhor opção para Facetas de Porcelana em SP </p>
<h2>A melhor opção para Facetas de Porcelana em SP </h2>
<p>Esperamos que você tire todas as suas dúvidas ao consultar nossos profissionais sobre cada tipo de serviço que possuímos em nossa clínica. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>