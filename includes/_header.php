<header itemscope itemtype="http://schema.org/Organization">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWMC6NB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div class="container">
        <div class="topo">
            <div class="col-md-10">
                <div class="tel">
                    <a title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">
                        <i class="fas fa-phone-alt"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["telefone"]; ?></span>
                    </a> 
                    |
                    <a title="whatsApp" href="https://api.whatsapp.com/send?phone=551196325-1772">
                        <i class="fab fa-whatsapp"></i><span itemprop="telephone"> (<?php echo $unidades[1]["ddd"]; ?>) <?php echo $unidades[1]["whatsapp"]; ?></span>
                    </a> 
                    |
                    <a title="E-mail" href="mailto:<?php echo $emailContato; ?>">
                        <i class="fas fa-envelope-open-text"></i><span itemprop="telephone"> <?php echo $emailContato; ?></span>
                    </a> 
                </div>
            </div>
            <div class="col-md-2">
                <div class="redes-topo">
                    <a href="<?php echo $url; ?>" target="_blank" title="Facebook <?php echo $nome_empresa; ?>"><i class="fab fa-facebook-f"></i></a>
                    <a href="<?php echo $url; ?>" target="_blank" title="Instagram <?php echo $nome_empresa; ?>"><i class="fab fa-instagram"></i></a>
                    <a href="<?php echo $url; ?>" target="_blank" title="Youtube <?php echo $nome_empresa; ?>"><i class="fab fa-youtube"></i></a>
                    <a href="<?php echo $url; ?>" target="_blank" title="Linkedin <?php echo $nome_empresa; ?>"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
    </div>  

    <div class="container header-container-main">
        <div class="col-md-2">
            <div class="logo">
                <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                    <span itemprop="image">
                        <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                    </span>
                </a>
            </div>
        </div>
        
        <div class="col-md-10">
            <nav class="menu">
                <ul class="menu-list">
                    <li><a href="https://reop.com.br/" title="Página inicial">Home</a></li>
                    <li><a href="https://reop.com.br/quem-somos/" title="Quem Somos">Quem Somos</a></li>
                    <li><a href="https://reop.com.br/centro-de-estetica/" title="Centro de Estética">Centro de Estética</a></li>
                    <li><a href="https://reop.com.br/tratamentos-odontologicos/" title="Tratamentos">Tratamentos</a></li>
                    <li><a href="https://reop.com.br/tratamentos-odontologicos/ortodontia/" title="Ortondontia">Ortondontia</a></li>
                    <li><a href="https://reop.com.br/parceiros-reop/" title="Parceiros">Parceiros</a></li>
                    <li><a href="https://reop.com.br/blog-2/">Blog</a></li>
                    <li><a href="<?php echo $url; ?>" title="Informações">Informações</a></li>
                    <li><a href="https://reop.com.br/contato/" title="Contato">Contato</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>