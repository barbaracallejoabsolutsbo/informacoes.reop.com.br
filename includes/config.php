<?php

    // Principais Dados do Cliente
$nome_empresa = "Reop";
$emailContato = "consultas@reop.com.br";

    // Parâmetros de Unidade
$unidades = array(
    1 => array(
        "nome" => "Reop",
        "rua" => "Av. Paulista, 1499 – Salas 23 e 24 ",
        "bairro" => "Bela Vista",
        "cidade" => "São Paulo",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "01311-200",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "11",
            "telefone" => "3262-1625",
            "whatsapp" => "96325-1772",
            "link_maps" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.1698408018124!2d-46.65805898575616!3d-23.56234286751201!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c92c6198bb%3A0x2be6435aa0a510ff!2sAv.%20Paulista%2C%201499%20-%20Bela%20Vista%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2001311-200!5e0!3m2!1spt-BR!2sbr!4v1625852801500!5m2!1spt-BR!2sbr" // Incorporar link do maps.google.com
        ),
    2 => array(
        "nome" => "",
        "rua" => "",
        "bairro" => "",
        "cidade" => "",
        "estado" => "",
        "uf" => "",
        "cep" => "",
        "ddd" => "",
        "telefone" => ""
    )
);

    // Parâmetros para URL
$padrao = new classPadrao(array(
        // URL local
    "http://localhost/informacoes.reop.com.br/",
        // URL online
    "https://www.informacoes.reop.com.br/"
));

    // Variáveis da head.php
$url = $padrao->url;
$canonical = $padrao->canonical;

    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "162.241.2.49";
    // $email_remetente         = "dispara-email@absolutsbo.com.br";
    // $senha_remetente         = "DisparaAbsolut123?";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
    
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Aparelho Dental Transparente",
        "Aparelho Dental",
        "Aparelho Dental Preço",
        "Aparelho Dentário Valor",
        "Aparelho Odontológico",
        "Aparelho Ortodôntico Auto ligado Preço",
        "Aparelho ortodôntico invisível",
        "Bichectomia",
        "Bichectomia preço",
        "Clareamento Dental Valor",
        "Clareamento Odontológico",
        "Clareamento Odontológico Preço",
        "Clínica de Ortodontia",
        "Clínica Odontológica Especializada",
        "Consultório Odontológico que Faz Bichectomia",
        "Dentista Urgente",
        "Emergência de Canal",
        "Emergência Odontológica",
        "Estética Dental Preço",
        "Extração de dente do siso",
        "Extração de dente do siso preço",
        "Tratamento de Endodontia",
        "Tratamento de Endodontia preço",
        "Facetas de Porcelana",
        "Facetas de Resina",
        "Harmonização do Rosto Valor",
        "Harmonização Facial Especializada",
        "Harmonização Facial Feminino",
        "Harmonização Facial Preço",
        "Implante de Dentes de Resina",
        "Implante Dentário",
        "Implante Odontológico",
        "Implantes de Dentes de Porcelana",
        "Lente de Contato de Porcelana",
        "Lente de Contato Dental",
        "Lente de Porcelana",
        "Lente de resina",
        "Lipo de Papada Preço",
        "Odontopediatria",
        "Odontopediatria Preço",
        "Periodontia",
        "Periodontia especializada",
        "Prótese Dentaria Fixa",
        "Prótese Dentária Preço",
        "Prótese Fixa Preço",
        "Prótese Protocolo Superior Preço",
        "Bruxismo",
        "Invisalign",
        "Invisalign preço",
        "Placa de bruxismo"
    );
    
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */