<?php
    $title       = "Odontopediatria em SP Preço";
    $description = "A odontopediatria em SP preço ideal e justo está na REOP. Somos uma clínica sólida e séria que não hesita em fazer um bom trabalho e oferece soluções completas aos clientes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A <strong>odontopediatria em SP preço</strong> ideal e justo está na REOP. Somos uma clínica sólida e séria que não hesita em fazer um bom trabalho e oferece soluções completas aos clientes. Desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos a risca.</p>
<p>Em suma, a <strong>odontopediatria em SP preço</strong> envolve diversos fatores, no entanto, nada mais é que uma especialidade dentro da odontologia, voltada principalmente para cuidar da saúde bucal, desde bebês até adolescentes. Dessa forma, o profissional da área é essencial para acompanhar todo o crescimento e a formação dentária das crianças, garantindo que o sorriso dos pequenos fique longe dos problemas. </p>
<h2><strong>Mais informações sobre odontopediatria em SP preço:</strong></h2>
<p>O profissional que trabalha no ramo de <strong>odontopediatria em SP preço</strong> tem todas as técnicas que um dentista convencional possui, porém, com foco no sorriso dos menores, que possui algumas particularidades.</p>
<p>Dentre as diversas características que englobam a <strong>odontopediatria em SP preço</strong>, podemos citar:</p>
<ul>
<li>         promoção de saúde, devendo o especialista educar bebês, crianças, adolescentes;</li>
<li>         prevenção em todos os níveis de atenção, devendo o especialista atuar sobre os problemas relativos à cárie dentária, ao traumatismo, à erosão, à doença periodontal, às mal-oclusões, entre outros;</li>
<li>         diagnosticar as alterações que afetam o sistema estomatognático e identificar fatores de risco em nível individual para os principais problemas da cavidade bucal;</li>
<li>         tratamento das lesões dos tecidos moles, dos dentes, dos arcos dentários e das estruturas ósseas adjacentes, decorrentes de cárie, traumatismos, erosão, doença periodontal, entre outros;</li>
<li>         condução psicológica dos bebês, crianças, adolescentes, e seus respectivos responsáveis para atenção odontológica.</li>
</ul>
<p>Lembrando que, o auxílio de um profissional de <strong>odontopediatria em SP preço</strong> é imprescindível na hora de escolher as ferramentas certas para o momento da escovação, de modo a garantir que o processo de higienização siga cuidadosamente as orientações do odontopediatra.</p>
<h2><strong>Odontopediatria em SP preço justo é na REOP!</strong></h2>
<p>Nós buscamos a excelência com humildade durante todo o contato com o paciente, explicando suas necessidades de uma forma didática, simples e clara. Além disso, estamos sempre disposto a ouvi-lo, fazer parte do cotidiano do público que atendemos, e por isso, somos reconhecidos como uma clínica de referência na área odontológica e interpessoal em que atua.</p>
<p>No momento em que entrar em contato conosco, você notará que encontrou a melhor <strong>odontopediatria em SP preço</strong>. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma clínica que prioriza e respeita você. Ligue agora mesmo e saiba mais.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>