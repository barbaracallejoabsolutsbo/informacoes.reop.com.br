<?php
    $title       = "Harmonização Facial Preço";
    $description = "Profissionais éticos e experientes, trabalhando com discrição e respeito a fim de buscar excelentes resultados em todos os seus desejos e vontades para sua harmonização facial preço bom.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Encontre <strong>harmonização facial preço</strong> acessível, com segurança e profissionalismo na REOP Odontologia e Estética. Localizados em São Paulo, na Avenida Paulista, promovemos qualidade de vida, reabilitando sorrisos, rejuvenescendo o rosto dos pacientes, também oferecendo serviços relaxantes para equilíbrio de corpo e mente. Com comprometimento, honestidade e bom relacionamento, trabalhamos com foco em encantar, proporcionando resultados que vão além da expectativa inicial de cada paciente.</p>
<p>Profissionais éticos e experientes, trabalhando com discrição e respeito a fim de buscar excelentes resultados em todos os seus desejos e vontades para sua <strong>harmonização facial preço</strong> bom. A REOP Odontologia e Estética se dedica inteiramente aos pacientes, unindo ética, competência e técnica para otimizar serviços para tratamentos odontológicos e estéticos para você.</p>
<p>A REOP tem como foco o atendimento personalizado, com toda prestatividade, interpretando todos desejos de cada paciente com comunicação gentil, competente e totalmente clara. Com nossa <strong>harmonização facial preço</strong> justo, interpretamos seus desejos de corrigir linhas e traços e encontramos as maneiras mais harmoniosas para preencher e definir linhas desejadas em sua face.</p>
<p>Com diversos diferenciais em nosso consultório, contamos com 5 salas de atendimento individualizadas, exames orais dentários com câmeras intra oral para check up digital, avisos periódicos via e-mail para checkup, rotinas de saúde bucal de 6 em 6 meses e acompanhamento odontológico geral, além de condições especiais para pagamento e facilidades para se organizar.</p>
<p>Fale com nosso atendimento e agende já sua <strong>harmonização facial preço</strong> bom disponível com a REOP Odontologia e Estética.</p>
<h2><strong>Agende sua avaliação e realize a harmonização facial preço mais acessível de São Paulo</strong></h2>
<p>Fale com nosso atendimento e agende já sua consulta para avaliação profissional e interpretação das suas expectativas com o procedimento de<strong> harmonização facial preço </strong>justo com a REOP.</p>
<p>Também é possível encontrar tratamentos odontológicos completos que vão desde a reabilitação oral até procedimentos de estética bucal. Conheça mais sobre nossos serviços acessando nosso site e fale com nosso atendimento para consultas e agendamentos.</p>
<h2><strong>Harmonização Facial preço bom com atendimento em consultório na Avenida Paulista</strong></h2>
<p>Estamos localizados na Avenida Paulista, número 1499, com consultório especializado em procedimentos odontológicos e estéticos. Conheça nossa clínica de harmonização facial preço justo e renove os seus traços faciais.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>