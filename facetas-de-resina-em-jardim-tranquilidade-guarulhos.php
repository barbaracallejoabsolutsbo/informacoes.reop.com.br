<?php
$title       = "Facetas de Resina em Jardim Tranquilidade - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>As Facetas de Resina em Jardim Tranquilidade - Guarulhos são restaurações ou placas de resina que são cimentadas aplicadas em cima da superfície dos dentes com formatos anatomicamente harmoniosos e confortáveis. Tem como objetivo principal corrigir variações de cor e de formato dos dentes. As Facetas de Resina em Jardim Tranquilidade - Guarulhos têm espessura maior que as lentes de contato, e precisam de pequeno desgaste para encaixe perfeito e confortável.</p>
<p>Contando com profissionais competentes e altamente capacitados no ramo de DENTISTA, a REOP ODONTO oferece a confiança e a qualidade que você procura quando falamos de Implante de Dentes de Resina, Odontopediatria Preço, Emergência de Canal, Aparelho Odontológico e Facetas de Porcelana. Ainda, com o mais acessível custo x benefício para quem busca Facetas de Resina em Jardim Tranquilidade - Guarulhos, uma vez que, somos a empresa que mais se desenvolve no mercado, mantendo o melhor para nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>