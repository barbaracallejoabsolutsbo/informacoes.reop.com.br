<?php
$title       = "Implantes de Dentes de Porcelana no Pacaembú";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existem diversas maneiras de se colocar um implante de dentes. A forma mais segura e funcional do que as próteses removíveis são os Implantes de Dentes de Porcelana no Pacaembú. Com resultados altamente satisfatórios, os implantes ficam fixos, firmes e rosqueados sobre o implante. Para realização do procedimento é necessário diagnóstico profissional e pequenas atenções pré e pós operatório. Agende sua consulta.</p>
<p>Como uma empresa de confiança no mercado de DENTISTA, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Implantes de Dentes de Porcelana no Pacaembú. A REOP ODONTO vem crescendo e mostrando seu potencial através de Prótese Fixa Preço, Invisalign preço, Extração de dente do siso preço, Dentista Urgente e Emergência de Canal, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>