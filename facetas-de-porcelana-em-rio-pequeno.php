<?php
$title       = "Facetas de Porcelana em Rio Pequeno";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Hoje em dia tratamentos com Facetas de Porcelana em Rio Pequeno estão dentre os mais procurados na maioria dos consultórios odontológicos. Trata-se de um processo que usa pequenas peças de porcelana moldadas com o formato dos seus dentes com objetivo de cobrir e revestir os seus dentes reais, dando aspecto de harmonia, bonitos e brancos. Encontre todas as opções de facetas que você procura conosco.</p>
<p>Como uma empresa de confiança no mercado de DENTISTA, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Facetas de Porcelana em Rio Pequeno. A REOP ODONTO vem crescendo e mostrando seu potencial através de Extração de dente do siso preço, Prótese Dentaria Fixa, Facetas de Porcelana, Clareamento Dental Valor e Lipo de Papada Preço, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>