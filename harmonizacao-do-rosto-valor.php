<?php
    $title       = "Harmonização do Rosto Valor";
    $description = "Nossos meios de contatos estão disponíveis para que você possa tirar suas dúvidas ou até mesmo fazer seu orçamento. Estamos sempre abertos para te receber como paciente em nossa clínica. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>É cada vez mais comum encontrarmos alguém em nosso cotidiano que tenha feito algum tipo de procedimento estético, principalmente a harmonização. É um procedimento que tem um resultado incrível, porém que não deve ser feita em qualquer lugar. Se você procura por esse tipo de serviço, garantimos que em nossa clínica, encontrará harmonização do rosto valor acessível. Nossos profissionais se qualificaram durante anos para que pudessem exercer suas devidas atividades, possuindo materiais em perfeito estado para que tudo ocorra como o seu planejado. Estamos há mais de 15 anos atuando como clínica, nos aprimorando cada vez mais para ajudar através de nossos serviços, cada um de nossos pacientes. Assim como em outras especialidades, apresentamos diversas formas para que a Harmonização do rosto valor baixo, seja algo dentro da sua realidade. </p>
<p>    Ao longo do procedimento da Harmonização do rosto valor baixo, priorizamos o seu conforto e privacidade em nossas salas de atendimentos individualizadas, que atualmente são mais de 4. Priorizamos sempre o bem estar de nossos clientes, para fazer com que além de buscar quando precisar, queiram estar conosco. E para isso, fazemos o melhor trabalho com isso com o nosso time de profissionais e todo o conjunto dos nossos funcionários da clínica. Confirmamos que após o seu procedimento de harmonização do rosto valor baixo, manteremos contato para fazer o seu pós acompanhamento. E para mantermos o comprometimento, mandaremos também e-mails lembrete de checkup para que o serviço seja extremamente completo. Caso você ache necessário, nosso endereço está disponível também em nosso site para você nos fazer uma visita. Nossa meta é sempre fazer com que a sua satisfação após consultar nossa empresa, seja alcançada e para isso temos profissionais que sempre estão a sua disposição para atender o que nos é pedido. Consultando Harmonização do rosto valor baixo em nossa clínica, você verá que possuímos grandes diferenciais que não fáceis de encontrar, além das mais variadas formas de pagamento para que você possa fazer o seu procedimento em qualquer condição financeira que você estiver. </p>

<h2>Mais detalhes sobre Harmonização do rosto valor acessível  </h2>
<p>Possuímos profissionais que estudaram durante anos para para que possam fazer todo o atendimento necessário em nossa clínica, principalmente em relação a Harmonização do rosto valor acessível, temos anos de experiência para te oferecer o melhor trabalho possível.  </p>
<h2> </h2>
<h2>A melhor opção para Harmonização do rosto valor baixo  </h2>
<p>Estamos disponíveis para te atender sempre que você nos consultar. Entre em contato conosco pelo e-mail ou até mesmo pelo Whatsapp, para que possamos sanar suas dúvidas sem você sair de casa. Ou nos visite, será um prazer te receber em nossa clínica.  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>