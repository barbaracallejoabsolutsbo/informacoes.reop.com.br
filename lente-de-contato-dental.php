<?php
    $title       = "Lente de Contato Dental";
    $description = "Conheça a lente de contato dental, grande aliado na restauração do brilho cor dos dentes, é um procedimento popular entre os famosos e celebridades do mundo todo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a <strong>lente de contato dental</strong>, grande aliado na restauração do brilho cor dos dentes, é um procedimento popular entre os famosos e celebridades do mundo todo. Com este tipo de procedimento estético é possível renovar a cor, restaurar o brilho e fazer mínimos ajustes anatômicos nos dentes dos pacientes. Conheça nossa clínica especializada em procedimentos como estes e muito mais.</p>
<p>A REOP Odontologia e Estética está localizada na Avenida Paulista, local de fácil acesso e muito simples de encontrar. Agende sua consulta conosco e venha fazer uma avaliação com nossos profissionais para encontrar soluções como a <strong>lente de contato dental</strong>. Trabalhamos com tratamentos odontológicos em geral que vão de clareamento dental, restaurações, microcirurgias, facetas diretas e indiretas, lentes de contato dentária em diversos materiais e muitos outros tratamentos.</p>
<p>Utilizada junto de harmonização facial, bichectomia, preenchimentos labiais, entre outros procedimentos estéticos, é possível atingir níveis de excelência nos procedimentos e alcançar resultados harmoniosos e incríveis. Acesse nosso site e conheça serviços exclusivos disponíveis na REOP Odontologia e Estética. Faça sua <strong>lente de contato dental</strong> e em algumas consultas conosco e rapidamente tenha um novo sorriso!</p>
<p>A <strong>lente de contato dental</strong> é um tratamento dos menos dolorosos, entregando os melhores resultados estéticos em questão de harmonia, naturalidade e dentes brancos com aparência natural. Com a REOP você encontra serviços e tratamentos estéticos para reabilitação oral, correção de alinhamento dos dentes, tratamentos para recuperação funcional da mastigação, estética para harmonização do rosto, pele e muito mais. Fale conosco e agende um atendimento para consulta e informações mais específicas.</p>
<h2><strong>Colocar lente de contato dental é um procedimento doloroso?</strong></h2>
<p>Não, a instalação da <strong>lente de contato dental</strong> tem sido um dos procedimentos mais procurados ultimamente. Indolor, as pequenas lentes são instaladas por cima dos dentes sem a necessidade de qualquer raspagem. Em porcelana e resina, aqui você terá suas lentes de contato rapidamente, com atendimento profissional e qualificado para procedimentos estéticos odontológicos em geral.</p>
<h2><strong>Quanto tempo demora para colocar lente de contato dental?</strong></h2>
<p>São feitos preparos, moldagens e fotografias com simulação e preparação de peças provisórias, além de tudo envolvido para o planejamento completo do projeto. Dependendo do material, o tempo pode ser maior, sendo a <strong>lente de contato dental</strong> em porcelana a mais difícil e demorada para confecção, já que dependem de um laboratório protético especializado. Em suma, no máximo em 30 dias o sorriso do paciente já está pronto para ser exposto com satisfação e harmonia.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>