<?php
    $title       = "Prótese Dentaria Fixa";
    $description = "Para que a reconstrução estética e funcional do dente danificado seja eficiente, muitas vezes é necessário utilizar uma prótese dentaria fixa.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para que a reconstrução estética e funcional do dente danificado seja eficiente, muitas vezes é necessário utilizar uma <strong>prótese dentaria fixa</strong>. Neste sentido, a REOP se destaca no ramo, pois, não hesita em fazer um bom trabalho e está focada em fazer o melhor possível sempre, com conforto, praticidade e cordialidade; otimizar o tempo de atendimento, interpretando o desejo das pessoas com uma linguagem gentil e clara.</p>
<p>A seleção entre os diversos tipos de <strong>prótese dentaria fixa</strong> pode ser complexa até mesmo para o dentista especializado. Vale salientar que, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece.</p>
<h2><strong>Informações sobre </strong><strong>prótese dentaria fixa:</strong></h2>
<p>Em suma, o uso de <strong>prótese dentaria fixa</strong> visa a substituição de um ou mais dentes ausentes, podendo ser apoiadas sobre implantes ou dentes que funcionam como pilares. </p>
<p>A <strong>prótese dentaria fixa</strong> é indicada quando existe um bom suporte ósseo e uma boa saúde periodontal. Caso o paciente não possua esses dois fatores favoráveis, essa alternativa pode trazer prejuízos para a saúde oral, assim, caberá ao dentista recomendar qual a melhor opção, de acordo com as especificações de cada paciente. A indicação do procedimento de colocação da <strong>prótese dentaria fixa</strong> a ser feito dependerá da análise de um dentista especializado.</p>
<p><br /> Por fim, terminado o tratamento, a durabilidade da <strong>prótese dentaria fixa</strong> é a informação que mais importa, pois, e a sua expectativa de vida é em média de vida de 8 anos para os dispositivos protéticos unitários, apresentando com boa adaptação aos dentes, esteticamente satisfatórias e também sem problemas como a infiltração por cárie dentária.</p>
<h2><strong>Conheça mais sobre a </strong><strong>prótese dentaria fixa da REOP</strong></h2>
<p>Primeiramente, nós respeitamos o nosso ambiente de trabalho e as pessoas com as quais atuamos. Estarmos unidos pelo bem comum prestando ótimo serviço com o reconhecimento de nossos pacientes, colegas e parceiros. Além disso, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição.</p>
<p>Na consulta inicial  o  profissional procura conhecer as necessidades e desejos do paciente e é realizado uma anamnese com um questionário de saúde, o exame com a câmera intra oral e fotos. Assim, com todo este material é apresentado um diagnóstico e um plano de tratamento adequado.</p>
<p>Diante de todas essas vantagens, nós ainda estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos a risca. Não perca mais tempo e nem a oportunidade de se tornar o nosso parceiro de longa data. Ligue agora mesmo e saiba mais.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>