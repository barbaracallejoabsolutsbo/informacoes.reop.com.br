<?php

$title       = "Galeria de Fotos";
    $description = ""; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "tools/fancybox",
        "galeria-fotos"
    ));
    
    ?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array($title)); ?>
            <h1><?php echo $h1;?></h1>
            <section>
                <h2>Galeria 1</h2>
                <div class="lista-galeria-fancy row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="http://localhost/_sitebase_padrao/imagens/item/item-1.jpg" title="Item - 1" data-fancybox-group="item">
                            <img src="http://localhost/_sitebase_padrao/imagens/item/item-thumb-1.jpg" alt="Item - 1" title="Item - 1" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="http://localhost/_sitebase_padrao/imagens/item/item-2.jpg" title="Item - 2" data-fancybox-group="item">
                            <img src="http://localhost/_sitebase_padrao/imagens/item/item-thumb-2.jpg" alt="Item - 2" title="Item - 2" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="http://localhost/_sitebase_padrao/imagens/item/item-3.jpg" title="Item - 3" data-fancybox-group="item">
                            <img src="http://localhost/_sitebase_padrao/imagens/item/item-thumb-3.jpg" alt="Item - 3" title="Item - 3" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <a href="http://localhost/_sitebase_padrao/imagens/item/item-4.jpg" title="Item - 4" data-fancybox-group="item">
                            <img src="http://localhost/_sitebase_padrao/imagens/item/item-thumb-4.jpg" alt="Item - 4" title="Item - 4" class="img-responsive">
                        </a>
                    </div>
                </div>          
            </section>
            <section>
                <h2>Galeria 2</h2>
                <?php echo $padrao->listaGaleria("Item", 4); ?>
            </section>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox"
    )); ?>

</body>
</html>