<?php
$title       = "Prótese Protocolo Superior Preço em Maia - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Prótese Protocolo Superior Preço em Maia - Guarulhos é uma das soluções mais modernas e definitivas para solução de reabilitação oral de pacientes que acabaram por perder todos ou quase todos os dentes. Normalmente funciona como prótese fixa composta principalmente por 12 dentes presos em uma estrutura de resina, parafusados em 4, 5 ou 6 parafusos, dando total conforto e dispensando as dentaduras ou próteses removíveis.</p>
<p>Além de sermos uma empresa especializada em Prótese Protocolo Superior Preço em Maia - Guarulhos disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Aparelho Odontológico, Bichectomia, Extração de dente do siso, Emergência de Canal e Bruxismo. Com a ampla experiência que a equipe REOP ODONTO possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de DENTISTA.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>