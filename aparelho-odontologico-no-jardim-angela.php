<?php
$title       = "Aparelho Odontológico no Jardim Ângela";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existem vários modelos de aparelhos para uso em tratamentos odontológicos. Os aparelhos fixos são recomendados para quase todos os desalinhamentos dentários, disponíveis em modelos metálicos, cerâmica ou transparente, requer disciplina do paciente para boa higiene e aparência durante o tratamento. Encontre Aparelho Odontológico no Jardim Ângela de qualidade com tratamento profissional na REOP Odontologia e Estética.</p>
<p>Especializada em Aparelho Odontológico no Jardim Ângela, a REOP ODONTO ganha destaque no mercado de DENTISTA em que atua com experiência e amplo conhecimento em diferentes vertentes como Emergência de Canal, Facetas de Resina, Implante Dentário, Harmonização Facial Preço e Bichectomia. Unindo profissionais qualificados para um atendimento de alto padrão e dedicação, nossa empresa, hoje, é destaque entre as melhores do ramo. Conte com os nossos profissionais para garantir o melhor em que busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>