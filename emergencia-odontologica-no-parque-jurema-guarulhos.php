<?php
$title       = "Emergência Odontológica no Parque Jurema - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está com uma Emergência Odontológica no Parque Jurema - Guarulhos de qualquer gravidade que seja e não sabe o que fazer, a REOP Odontologia e Estética pode lhe dar todo o suporte. Com serviços emergenciais para todo o ramo da odontologia, aqui você tem preço justo e atendimento profissional e atencioso com fácil acesso em São Paulo. Entre em contato conosco para mais informações e encontre uma solução para sua emergência.</p>
<p>Você procura por Emergência Odontológica no Parque Jurema - Guarulhos? Contar com empresas especializadas no segmento de DENTISTA é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa REOP ODONTO é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Lipo de Papada Preço, Harmonização Facial Feminino, Lente de Contato de Porcelana, Implante Dentário e Harmonização do Rosto Valor e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>