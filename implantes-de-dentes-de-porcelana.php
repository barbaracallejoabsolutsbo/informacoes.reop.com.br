<?php
    $title       = "Implantes de Dentes de Porcelana";
    $description = "Os implantes de dentes de porcelana são muito populares hoje em dia, principalmente quando o objetivo é buscar um aspecto visual bem natural com muita durabilidade.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Faça seus <strong>implantes de dentes de porcelana</strong> com a REOP Odontologia e Estética. Entre em contato conosco agora mesmo para agendar uma avaliação com nossos doutores e encontrar os tratamentos mais adequados. Conte conosco para criar próteses especiais e especialmente adaptadas para você e ter todo suporte para cirurgia de implantes de dentes de resina ou porcelana, idênticos aos seus dentes normais.</p>
<p>Os <strong>implantes de dentes de porcelana</strong> são muito populares hoje em dia, principalmente quando o objetivo é buscar um aspecto visual bem natural com muita durabilidade. Dentes de porcelana são um pouco mais duráveis e resistentes que dentes de resina, por esse motivo, seu valor é um pouco mais elevado. Seus resultados são excelentes e pelo material de alta qualidade, acumulam muito menos resíduos que os demais materiais encontrados por preços menores.</p>
<p>O procedimento para <strong>implantes de dentes de porcelana</strong> conta com algumas fases, que vão desde a cirurgia de implante do pino de titânio na cavidade óssea para fixação da prótese que é a fase final. Durante todo esse processo, que pode levar de 8 meses até 2 anos dependendo do caso de cada paciente, acontece um fenômeno chamado de ósseo integração, onde o material de titânio fixado se integra com a estrutura óssea do paciente, aumentando a fixação e resistência do implante. Durante esse tempo, o laboratório especializado confecciona sua prótese adaptada sob medida para você.</p>
<p>Mas caso eu opte por realizar o procedimento de <strong>implantes de dentes de porcelana</strong>, ficarei todo esse tempo sem dentes? A resposta para esta dúvida frequente é não. Quando este tipo de procedimento é realizado, diversos fatores são analisados previamente para criar o ambiente perfeito para o implante dentário. Podem ser encontradas diversas soluções provisórias para pelo menos manter o aspecto visual agradável para o paciente e o mais funcional dentro do possível.</p>
<h2><strong>Implantes de dentes de porcelana em São Paulo é com a REOP Odontologia</strong></h2>
<p>A REOP Odontologia e Estética está localizada em São Paulo, na Avenida Paulista, em uma das principais avenidas da cidade. Com preço justo e todo profissionalismo, faça seus <strong>implantes de dentes de porcelana </strong>conosco e supere expectativas com resultados incríveis.</p>
<h2><strong>Implantes de dentes de porcelana, solução de alta qualidade para reabilitação oral</strong></h2>
<p>Os dentes de porcelana são uma das soluções mais eficientes para reabilitação oral. Com melhor compatibilidade com o organismo, acúmulo totalmente reduzido de resíduos, brilho e cor por muito mais tempo, os<strong> implantes de dentes de porcelana</strong> podem ser feitos com a REOP Odontologia. Agende sua consulta e conheça mais sobre nossos serviços e tratamentos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>