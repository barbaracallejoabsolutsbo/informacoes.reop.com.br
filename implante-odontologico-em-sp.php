<?php
    $title       = "Implante Odontológico em SP";
    $description = "Com a REOP Odontologia e Estética você encontra profissionais para te acompanhar durante todo o processo de implante odontológico em SP.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está procurando por clínicas que realizam cirurgia e procedimentos para <strong>implante odontológico em SP</strong>, a REOP Odontologia é sua solução. Localizados na Avenida Paulista, oferecemos serviços especializados de odontologia e estética com profissionalismo e excelência.</p>
<p>O <strong>implante odontológico em SP</strong> restaura seu sorriso, devolve sua autoestima, sendo seguro e acessível com a REOP Odontologia e Estética. Fale conosco e encontre condições com preços especiais para cirurgia de instalação de implantes odontológicos e confecção de próteses perfeitas e harmoniosas. Totalmente biocompatíveis, os implantes de titânio instalados servem como uma nova raiz para fixação de suas próteses unitárias ou para vários dentes. Marque uma consulta para atendimento profissional e avaliação clínica.</p>
<p>A parte cirúrgica para <strong>implante odontológico em SP é</strong> um procedimento que fixa o implante de titânio que futuramente irá sustentar a prótese que virá do laboratório, feita sob medida para você. Ele é instalado no osso e após integração óssea é colocada a prótese definitiva. O valor para o implante varia também de acordo com o material que irá compor a prótese, podendo ser de resina ou porcelana, além de outros materiais pouco comuns utilizados nos dias de hoje, mas que não deixam de ser um opcional a consultar com nossos doutores.</p>
<p>Casos mais simples de <strong>implante odontológico em SP</strong> podem demorar prazos de até 8 meses para finalização completa de adaptação, osteointegração e cicatrização geral. Em casos mais severos de degeneração óssea, o processo pode levar até 2 anos para finalização.</p>
<h2><strong>Acompanhamento profissional completo para implante odontológico em SP</strong></h2>
<p>Com a REOP Odontologia e Estética você encontra profissionais para te acompanhar durante todo o processo de<strong> implante odontológico em SP</strong>. Venha fazer uma avaliação para identificarmos a situação atual e quais tratamentos devem ser feitos até a fixação final da prótese em você. Reabilite seu sorriso conosco e tenha toda atenção de nossos profissionais sempre que precisar!</p>
<h2><strong>Tipos de próteses para implante odontológico em SP</strong></h2>
<p>Os tipos de próteses que são feitas para o <strong>implante odontológico em SP,</strong> normalmente são em porcelana ou resina, por se assemelhar visualmente com a cor e brilho dos dentes normais, serem resistentes e possuírem boa dureza para uso cotidiano. Também podem ser encontradas próteses dentárias em outros materiais como níquel, ouro e outros materiais odontológicos biocompatíveis. Encontre o material mais indicado para seu caso e que agrade o seu gosto para implantes odontológicos e faça seus procedimentos e tratamentos com o melhor consultório odontológico de SP.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>