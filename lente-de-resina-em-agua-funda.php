<?php
$title       = "Lente de resina em Água Funda";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A lente de contato dental pode ser confeccionada em diversos tipos de materiais. Os mais comuns e estéticos utilizados são a resina e a porcelana. Anatomicamente desenvolvidos para serem confortáveis, de fácil instalação e fixação sem dor ou desgastes nos dentes, a Lente de resina em Água Funda é uma opção com custo mais acessível e resultados tão satisfatórios quanto os de porcelana, apenas diferindo em pequenos aspectos.</p>
<p>Desempenhando uma das melhores assessorias do segmento de DENTISTA, a REOP ODONTO se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Lente de resina em Água Funda com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Dentista Urgente, Harmonização do Rosto Valor, Clareamento Odontológico Preço, Aparelho ortodôntico invisível e Odontopediatria Preço, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>