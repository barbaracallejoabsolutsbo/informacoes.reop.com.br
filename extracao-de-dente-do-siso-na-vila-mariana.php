<?php
$title       = "Extração de dente do siso na Vila Mariana";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Extração de dente do siso na Vila Mariana é uma microcirurgia realizada por um cirurgião dentista que tem como principal objetivo remover os dentes do siso que normalmente não tem função e pode causar incômodos, nascer torto e acabar prejudicando o desenvolvimento da arcada dentária. É um procedimento relativamente rápido e indolor, sendo notado alguns incômodos apenas no pós-operatório, mas com o paciente medicado são toleráveis.</p>
<p>Especialista no mercado, a REOP ODONTO é uma empresa que ganha visibilidade quando se trata de Extração de dente do siso na Vila Mariana, já que possui mão de obra especializada em Bichectomia preço, Aparelho Dental, Prótese Dentária Preço, Facetas de Resina e Consultório Odontológico que Faz Bichectomia. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de DENTISTA, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>