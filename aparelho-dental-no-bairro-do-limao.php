<?php
$title       = "Aparelho Dental no Bairro do Limão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O aparelho ortodôntico ou Aparelho Dental no Bairro do Limão varia conforme o tipo de tratamento e correção a ser feito no paciente. Para fins estéticos e funcionais de seus dentes, existem aparelhos fixos metálicos, transparentes, placas e muito mais. Consulte nossos serviços e tratamentos e agende uma consulta para uma análise profissional e informações mais precisas sobre quais tratamentos são os mais adequados.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Aparelho Dental no Bairro do Limão? A REOP ODONTO é o que você precisa! Sendo uma das principais empresas do ramo de DENTISTA consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Implante Odontológico, Invisalign, Clareamento Odontológico Preço, Tratamento de Endodontia e Emergência Odontológica. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>