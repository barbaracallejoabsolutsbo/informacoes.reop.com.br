<?php
$title       = "Clínica Odontológica Especializada em Bela Vista - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Conheça a REOP Odontologia e Estética, Clínica Odontológica Especializada em Bela Vista - Guarulhos em procedimentos estéticos, próteses, restaurações, implantes e muito mais. Tratamentos de alinhamento com aparelho ortodôntico, clareamento a laser ou a gel, lentes de contato, coroas e facetas. Agende sua consulta com nossa equipe profissional e garanta o melhor serviço para você.</p>
<p>Na busca por uma empresa qualificada em DENTISTA, a REOP ODONTO será sempre a escolha com as melhores vantagens para o que você vem buscando. Além de fornecedor Bichectomia preço, Clínica de Ortodontia, Clínica Odontológica Especializada, Lente de Contato de Porcelana e Clareamento Dental Valor com o melhor custo x benefício da região, nós temos como missão garantir agilidade, qualidade e dedicação no que vem realizando para garantir a satisfação de seus clientes que buscam por Clínica Odontológica Especializada em Bela Vista - Guarulhos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>