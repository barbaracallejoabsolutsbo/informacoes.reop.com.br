<?php
    $title       = "Lente de Contato de Porcelana";
    $description = "O procedimento para aplicar lente de contato de porcelana conosco é indolor com resultados estéticos naturais e perfeitos. Entre em nosso site e saiba mais!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está procurando alguma estética dental que faça <strong>lente de contato de porcelana</strong> em São Paulo, a REOP Odontologia é uma clínica especializada em procedimentos estéticos e clínicos odontológicos. Veja nossos serviços exclusivos e marque sua consulta com nossos doutores. Com localização fácil de achar, estamos na avenida Paulista. Fale conosco e agende sua visita em nosso consultório.</p>
<p>Dentes tortos, com anatomia ruim ou quebrados, amarelados ou manchados, certamente incomodam você. A REOP possui tratamentos disponíveis para transformação completa do seu sorriso, aumentando sua autoestima. A REOP Odontologia e Estética, com todo cuidado e atenção, conserta imperfeições no seu sorriso com total compromisso. Agende sua consulta e tenha acompanhamento completo para moldar, criar e instalar com toda adaptação, sua <strong>lente de contato de porcelana </strong>conosco.</p>
<p>A <strong>lente de contato de porcelana </strong>não precisa de grande desgaste no dente para ser fixada. São estruturas extremamente finas que são aplicadas diretamente sobre seu dente, sem dor ou incômodo. Versáteis, hoje em dia são as maiores aliadas para correção de cor e brilho dos dentes, criando um sorriso perfeito, permitindo correções leves em questões anatômicas para os dentes do paciente.</p>
<p>Fale conosco e reserve seu horário. Saiba mais sobre todo o procedimento desde a fabricação até a instalação da sua <strong>lente de contato de porcelana </strong>com nossa clínica odontológica e estética.</p>
<h2><strong>Colocar lente de contato de porcelana é doloroso?</strong></h2>
<p>O procedimento para aplicar <strong>lente de contato de porcelana </strong>conosco é indolor com resultados estéticos naturais e perfeitos. As lentes são confeccionadas para serem fixadas por cima do dente sem desgaste, dando um aspecto harmônico e natural. Agende já uma consulta com nossa clínica e encontre procedimentos estéticos odontológicos como este, completo, desde a moldagem, confecção até a instalação de facetas e lentes de contato.</p>
<h2><strong>Diferenças entre as facetas e a lente de contato de porcelana</strong></h2>
<p>As facetas são feitas em porcelana ou resina. São mais grossas e corrigem a anatomia, cor e mínimos desalinhamentos nos dentes dos pacientes. São um pouco mais espessas que as lentes de contato, portanto é necessário pequeno desgaste para fixar. Diferente das facetas, a <strong>lente de contato de porcelana</strong> é muito fina e vai aplicada diretamente sobre os dentes, com espessura de no máximo 0,3mm, sem dores ou incômodos. Encontre as melhores opções de tratamento para você na REOP Odontologia e Estética.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>