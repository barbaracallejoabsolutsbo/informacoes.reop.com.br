<?php

    $title       = "Envia Contato";
    $description = "Mensagem enviada com sucesso."; // Manter entre 130 a 160 caracteres
    $h1          = "Confirmação de Contato";
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php"; 
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "mapa-site"
    ));

?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container text-justify">
            <?php echo $padrao->breadcrumb(array($title)); ?>
            <h1><?php echo $h1; ?></h1>
            <br>
            <h2 class="text-center">Contato Enviado com Sucesso!</h2>
            <br>
            <p>Continue navegando em nosso site, selecione uma página abaixo.</p>
            <div class="sitemap">
                <?php include "includes/estrutura-site.php"; ?>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>