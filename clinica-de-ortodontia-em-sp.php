<?php
    $title       = "Clínica de Ortodontia em SP";
    $description = "Mantenha contato conosco. Nossa equipe sempre estará disponível através das nossas redes socais e e-mail para te atender quando precisar. Não deixe de fazer seu orçamento!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você busca por Clínica de Ortodontia em SP, a Reop é o melhor lugar para você solucionar esse incômodo que te atrapalha quando você está comendo algo ou até mesmo conversando. A Clínica Reop é a melhor opção de lugar onde você possa consultar esse serviço, pois somos uma Clínica de Ortodontia em SP de extrema qualidade, com profissionais que atuam há anos nesse ramo tendo também estrutura qualificada para atender qualquer tipo de serviço. Buscamos satisfazer nossos clientes há 20 anos atuando como Clinica de Ortodontia para que cada vez mais possamos melhorar vidas de uma forma única e especial. </p>
<p> Procurando por Clinica de Ortodontia em SP acreditamos que, o que você mais almeja é melhorar a sua saúde e estética dental, para não ter cada vez mais preocupação enquanto os anos se passam e poder levar a vida de uma forma mais tranquila em relação aos seus dentes. A estética dos nossos dentes é importante, mas a saúde vem em primeiro lugar. Quando sentir qualquer tipo de dor na região bucal, não deixe de procurar nossa Clinica de Ortodontia em SP para que o seu problema seja resolvido da maneira mais rápida possível, com nossos equipamentos de qualidade, para também te poupar de toda dor desnecessária. Fazemos a questão de mantermos o contato com todos nossos clientes desde o momento da chegada na clínica, até mesmo após o tratamento ortodôntico e garantimos que em todas as fases do mesmo, não deixamos você desamparado para tirar dúvidas ou até mesmo querer fazer algum tipo de mudança no processo.  </p>
<p>Não deixe de nos consultar quando sentir qualquer tipo de dor em seus dentes, ou até mesmo para fazermos uma avaliação. Possuímos salas individuais para atender cada um de nossos clientes com privacidade e total atenção. Nossa Clinica de Ortodontia em SP está há mais de 15 anos fazendo serviços cada vez mais qualificados, com profissionais que possuem diploma para que possam exercer suas devidas profissões com grande responsabilidade e cautela. Estaremos sempre dispostos a te atender ou até mesmo para realizarmos um orçamento dentro do tratamento que você está precisando com as melhores condições de preços possíveis. </p>

<h2>Mais detalhes sobre Clinica de Ortodontia em SP  </h2>
<p>Pra mais esclarecimentos sobre Clinica de Ortodontia em SP, procure nossa Clínica Reop. Nosso objetivo é esclarecer todos os seus questionamentos o quão antes possível. Nosso e-mail e Whatsapp são de fácil acesso para que possamos sempre manter contato. </p>

<h2>A melhor opção para Clinica de Ortodontia em SP </h2>
<p>Temos o dever de fazer com que você saia de nossa clínica extremamente satisfeito, superando suas expectativas. Esperamos sempre te manter como nosso paciente para que juntos tornemos sua vida melhor.  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>