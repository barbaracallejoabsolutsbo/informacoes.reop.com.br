<?php
$title       = "Extração de dente do siso em Invernada - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Sim, além de um procedimento muito comum, o dente do siso é um dente sem função notável na arcada dentária e pode carear, causar incômodos enquanto nasce e inclusive nascer torto ou encavalado. Para esses últimos casos, a única solução é a Extração de dente do siso em Invernada - Guarulhos, um procedimento rápido e tranquilo, que requer apenas pequenas atenções no pós-operatório por um período curto, com resultado muito satisfatório.</p>
<p>Como uma especialista em DENTISTA, a REOP ODONTO se destaca dentre as demais empresas quando o assunto é Extração de dente do siso em Invernada - Guarulhos, uma vez que, nossa empresa conta com mão de obra com amplo conhecimento em diferentes ramificações do segmento, assim como em Aparelho Ortodôntico Auto ligado Preço, Extração de dente do siso preço, Prótese Dentária Preço, Implantes de Dentes de Porcelana e Lipo de Papada Preço. Temos os mais competentes profissionais e as principais ferramentas do mercado de modo a prestar o melhor atendimento possível.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>