<?php
$title       = "Periodontia especializada no Jaraguá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Encontre tratamentos exclusivos pela nossa clínica com Periodontia especializada no Jaraguá. Localizados em São Paulo, bem no centro da Avenida Paulista, aqui você tem tratamentos periódicos ou consultas avulsas. Agende um horário com um de nossos profissionais e tenha serviços e tratamentos de qualidade. Conheça também os serviços do ramo estético que nossa clínica oferece.</p>
<p>Desempenhando uma das melhores assessorias do segmento de DENTISTA, a REOP ODONTO se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Periodontia especializada no Jaraguá com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Implante de Dentes de Resina, Prótese Fixa Preço, Harmonização Facial Especializada, Periodontia especializada e Dentista Urgente, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>