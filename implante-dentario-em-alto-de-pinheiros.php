<?php
$title       = "Implante Dentário em Alto de Pinheiros";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os implantes que substituem um ou mais dentes perdidos são confeccionados em titânio biocompatível. Em outras palavras, implantes dentários são novas raízes para fixação de dentes em resina ou porcelana, ou até mesmo em ouro, níquel ou prata. O processo de instalação para resultados mais satisfatórios é uma microcirurgia, ou também encontrado em forma de prótese. Adquira Implante Dentário em Alto de Pinheiros com valor justo aqui.</p>
<p>Empresa pioneira no mercado de DENTISTA, a REOP ODONTO é qualificada e experiente quando o assunto são Implantes de Dentes de Porcelana, Harmonização do Rosto Valor, Prótese Protocolo Superior Preço, Prótese Dentária Preço e Clareamento Odontológico. Aqui você encontra soluções personalizadas para o que necessita e tem acesso ao que há de melhor e mais moderno para Implante Dentário em Alto de Pinheiros sempre com muita eficiência e qualidade para garantir a sua satisfação e sua fidelidade conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>