<?php
    $title       = "Consultório Odontológico que Faz Bichectomia";
    $description = "Faça seu orçamento conosco online sem precisar sair de casa, através do nosso Whatsapp que está disponível em nosso site. Nossa equipe está sempre disponível para tirar todas as suas dúvidas possíveis ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>  Procedimentos estéticos estão cada vez mais comuns em nosso cotidiano. Mas o número de pessoas que o deixam de fazer por conta do medo de o procedimento falhar, é muto alto também. Em nosso Consultório Odontológico que faz Bichectomia, garantimos que há somente profissionais qualificadas para que exerçam essa função tão importante. Afinal, é algo que afeta principalmente na autoestima da pessoa que procura esse procedimento e que exige total conhecimento da pessoa que está efetuando tal função. A clínica Reop é sua melhor opção para esse tipo de busca, pois estamos há mais de 15 anos exercendo essa funcionalidade dentro do nosso Consultório Odontológico que faz Bichectomia e possuímos profissionais em nosso consultório com anos de experiência nessa especialidade.  </p>

<p>Quando pensar em Consultório Odontológico que faz Bichectomia, a melhor alternativa para tirar todas as suas dúvidas sobre esse procedimento, em um lugar seguro e com profissional experiente no assunto, é a Reop. Estamos aqui para sanar todas as suas dúvidas, auxiliando em todo o processo., fazendo um pós acompanhamento também, mandando nosso e-mail lembrete de checkup e buscando sempre saber sua satisfação com o nosso serviço. Estamos sempre dispostos a saber o seu ponto de vista sobre nossa qualidade de serviço, por isso também fazemos questão de manter contato através do nosso Whatsapp. Nosso maior objetivo sempre, é melhorar a vida de nossos clientes da melhor maneira possível através dos nossos trabalhos e fazer com que sempre que for necessário, nossos clientes queiram correr ao nosso Consultório Odontológico que faz Bichectomia, pois sabem que na Reop serão bem atendidos com a maior atenção possível. Garantimos isso também através de nossos equipamentos qualificados para cada tipo de função e nossas mais de 4 salas de atendimentos individualizados priorizando também a sua privacidade e conforto em nosso Consultório Odontológico que faz Bichectomia  </p>

<h2>Mais detalhes sobre Consultório Odontológico que faz Bichectomia  </h2>
<p>Para tirar mais dúvidas sobre Consultório Odontológico que faz Bichectomia, estamos disponíveis de segunda à sexta através dos telefones de contato que possui em nosso site. Se preferir, pode nos consultar através do Whatsapp tambpem. Agradecemos o seu contato. </p>

<h2>A melhor opção para Consultório Odontológico que faz Bichectomia  </h2>
<p>Esperamos que à partir do momento que você entrar em contato com a nossa clínica, todas as suas necessidades sejam correspondidas pela nossa quipe de profissionais que estão a todo momento dispostos a te ajudar no que for preciso. Seja para tirar dúvidas sobre o procedimento, ou para fazermos um orçamento que seja extremamente acessível a você.  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>