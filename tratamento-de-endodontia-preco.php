<?php
    $title       = "Tratamento de Endodontia preço";
    $description = "O tratamento de endodontia preço adequado e justo está na REOP. Sabemos que está área é muito importante para a saúde da boca, principalmente para o sucesso dos tratamentos de reabilitação oral.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O <strong>tratamento de endodontia preço</strong> adequado e justo está na REOP. Sabemos que está área é muito importante para a saúde da boca, principalmente para o sucesso dos tratamentos de reabilitação oral.</p>
<p>Em geral, o <strong>tratamento de endodontia preço</strong> é uma especialidade destinada ao diagnóstico, tratamento e prevenção das doenças que atingem a parte interna do dente. Por isso, contamos com profissionais qualificados para não somente atender a sua necessidade, mas também, superar as suas expectativas apresentando soluções completas, bem como um atendimento personalizado.</p>
<h2><strong>Mais sobre </strong><strong>tratamento de endodontia preço:</strong></h2>
<p>De uma maneira geral, a intervenção do endodontista para o <strong>tratamento de endodontia preço</strong> é necessária para prevenir a ocorrência de infecções graves e para evitar a necrose completa, levando à perda dentária.</p>
<p>O profissional que realiza o <strong>tratamento de endodontia preço</strong> busca fazer com que o paciente se sinta mais confortável, e deve conversar bastante para que comece uma relação de confiança e segurança entre ele e o paciente. É preciso que o paciente se sinta seguro para realizar os procedimentos e confie na expertise do profissional, pois muitas vezes o paciente prefere extrair e perder o dente, por falta de conhecimento nos procedimentos, inclusive no <strong>tratamento de endodontia preço</strong>.</p>
<p>Entre outras complicações, o <strong>tratamento de endodontia preço</strong> pode evitar a ocorrência de quadros sistêmicos de infecção, que surgem quando as bactérias presentes na cavidade bucal chegam à corrente sanguínea. Além disso, o profissional da área. Vale lembrar que, o endodontista também cuida da estética e autoestima, pois realiza clareamento interno de dentes que escureceram</p>
<h2><strong>Tratamento de endodontia preço justo é com a REOP!</strong></h2>
<p>Há mais de 20 anos atuando com odontologia e estética, nós buscamos a excelência com humildade durante todo o contato com o paciente, explicando suas necessidades de uma forma didática, simples e clara. E ainda, desde o início, é estabelecida uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos a risca.</p>
<p>E ainda, pensando no bem estar completo de nossos clientes, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua contratação. Nós otimizamos o tempo de atendimento, interpretando o desejo das pessoas com uma linguagem gentil e clara. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma empresa que prioriza e respeita você. Ligue agora mesmo, tire todas as suas dúvidas e realize um orçamento sem compromisso.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>