<?php
    $title       = "Aparelho Dental em SP";
    $description = "Além de ótimos preços para colocar aparelho dental em SP, oferecemos diversas especialidades da odontologia e estética para que você realize todos os tratamentos que queira em um só lugar.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você procura pelo melhor lugar para cotar <strong>aparelho dental em SP </strong>encontrou o local certo. A REOP odontologia e estética é uma clínica completa que busca oferecer para os moradores da cidade de São Paulo procedimentos odontológicos e estéticos com preço justo e alta qualidade.  Como uma clínica completa oferecemos as melhores opções de tratamentos de saúde e estética para nossos clientes. O nosso <strong>aparelho dental em SP </strong>possui preço competitivo para a região além da alta qualidade de quem está há mais de 20 anos dentro do ramo da odontologia.  Nossa missão é promover qualidade de vida, cuidando e otimizando o dia a dia das pessoas. A Ortodontia é um segmento muito importante da odontologia principalmente quando o tratamento é realizado desde criança promovendo o alinhamento saudável dos dentes com o posicionamento correto.  Não perca tempo, agende já sua avaliação e tenha a melhor proposta de tratamento para cuidar de sua saúde e estética bucal.</p>
<p>O alinhamento dentário que o nosso <strong>aparelho dental em SP </strong>proporciona diversos benefícios de qualidade de vida como melhorar a mastigação, a estética dos dentes e até mesmo prevenir problemas gástricos. O sorriso é uma das principais impressões que temos de uma pessoa ao conhecê-la, por isso diversas pessoas podem sofrer até mesmo problemas de autoestima por conta da estética dentária. Volte a sorrir sem medo com a correção de nosso <strong>aparelho dental em SP. </strong>Oferecemos ótimas condições com odontologia acessível para todos. Possuímos diversos profissionais especializados em diferentes áreas de atuação para oferecer um melhor atendimento para nossos clientes. Os melhores resultados são conquistados pela alta dedicação de nossos profissionais em seu trabalho e em atualizar constantemente seus recursos mantendo o estudo de tecnologias que chegam para auxiliar a odontologia.</p>
<h2><strong>O melhor lugar para colocar aparelho dental em SP.</strong></h2>
<p>Além de ótimos preços para colocar <strong>aparelho dental em SP</strong>, oferecemos diversas especialidades da odontologia e estética para que você realize todos os tratamentos que queira em um só lugar. Com a meta de ser referência no segmento odontológico buscamos oferecer sempre o melhor atendimento para nossos clientes com conforto, praticidade e cordialidade.</p>
<h2><strong>Saiba mais sobre o nosso aparelho dental em SP.</strong></h2>
<p>Para saber mais sobre nosso <strong>aparelho dental em SP </strong>entre em contato e agende sua avaliação conosco. São 5 salas de atendimento individuais, check-up digital com exame dentário com câmera intra-oral, lembrete de consulta e muito mais.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>