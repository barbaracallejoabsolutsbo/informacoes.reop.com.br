<?php
$title       = "Prótese Dentaria Fixa na Cidade Ademar";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para colocação de prótese dentária fixa é necessário passar por algumas fases de análise e de instalação e cicatrização. Assim como outros procedimentos estéticos, trata de uma microcirurgia onde é implantado uma raiz para seu dente ser fixado, em titânio biocompatível. Assim que cicatrizado, tempo que varia de 2 a 6 meses, sua prótese pode ser fixada por rosca que é utilizada normalmente</p>
<p>Entre em contato com a REOP ODONTO se você busca por Prótese Dentaria Fixa na Cidade Ademar. Somos uma empresa especializada com foco em Tratamento de Endodontia, Aparelho Dental Transparente, Invisalign preço, Periodontia e Bichectomia onde garantimos o melhor para nossos clientes, uma vez que, contamos com o conhecimento adequado para o ramo de DENTISTA. Entre em contato e faça um orçamento com um de nossos especialistas e garanta o melhor custo x benefício do mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>