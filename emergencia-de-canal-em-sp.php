<?php
    $title       = "Emergência de Canal em SP";
    $description = "Estamos dispostos a te atender no horário em que nos chamar. Não deixe de manter contato conosco por um de nossos telefones, ou até mesmo pelo nosso whatsapp.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está buscando por Emergência de Canal em SP, a clínica Reop é o lugar ideal para você. Pois estamos há 20 anos mantendo diversas especialidades, para que possamos atender a qualquer tipo de tratamento dental. Ao sentir determinada dor a qual você sabe que é necessário procurar por Emergência de Canal em SP, não exite em manter algum contato conosco pois nossos profissionais são de mais alta qualidade para te atender com a maior atenção possível. Profissionais os quais estudaram anos para possuírem seus devidos cargos e então fazer um serviço de qualidade para que possamos resolver qualquer tipo de problema bucal em nossos pacientes.  </p>
<p>       Conviver com a dor de quando precisa-se de um tratamento canal é quase impossível, por isso ao sentir qualquer sinal desse tipo de aflição procure com urgência uma Emergência de Canal em SP. Em nossa clínica, assim que o procedimento for concluído fazemos questão de fazer um acompanhamento caso seja necessário seu retorno em nossa clínica. Enviamos e-mails de lembretes de checkup para que o seu compromisso seja marcado conosco e o seu problema seja resolvido o mais rápido possível. Mesmo com o tratamento de urgência, possuímos salas de atendimento individualizadas para que você receba todo o apoio necessário. Nosso maior e único objetivo é fazer com que você se satisfaça com o nosso serviço e principalmente fazer com que você viva a sua vida de uma maneira leve e sem preocupações ao falar, sorrir ou comer.  </p>
<p>        Certificamos que ao procurar Emergência de Canal em SP, em nossa clínica o orçamento que fizer será acessível na hora em que você precisar, pois damos também formas variadas de pagamentos, como até mesmo o parcelamento. Não receie em nos ligar, ou até mesmo nos chamar pelo Whatsap quando precisar de Emergência de Canal em SP, nossa equipe está disposta para te ajudar com o recurso que você tiver, na hora em que precisar.  </p>
<h2>      Mais detalhes sobre Emergência de Canal em SP </h2>
<p>Ao precisar de Emergência de Canal em SP, nossos profissionais com recursos apropriados estão devidamente preparados para te receber em nossa clínica. Afinal, são anos de experiência no mesmo ramo, aprimorando cada vez mais seus conhecimentos.  </p>
<h2>   A melhor opção para Emergência de Canal em SP </h2>
<p>Você pode nos consultar a qualquer hora, pelo número que está em nosso site ou até mesmo pelo nosso Whatsapp. Com preço acessível ao seu financeiro, garantimos te entregar um trabalho e atendimento de qualidade a qualquer hora.  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>